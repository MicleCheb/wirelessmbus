unit wmbus;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, strutils, Dialogs;

// Hello?world!            U2FsdGVkX193jMXETFHq0/yDhXbD6o4eIRMz6S3nlrg=
const
  // Block0
  L_BLOCK0 = 12;
  oLF = 0;
  oCF = 1;
  oMANIDF = 2;
  oADDRF = 4;
  oVERF = 8;
  oTYPEF = 9;
  oCRC0 = 10;
  // Block1
  L_BLOCK1 = 18;
  oCIF = 12;  // 0x78
  PACKETMAX = 255;

type
  TwmbCMD = record
    CMDname: string;
    CMDhex: byte;
    CMDhint: string;
  end;

  TwmbPacket = record
    // Block0
    LF: byte;
    CF: byte;
    MANIDF: word;
    ADDRF: integer;
    VERF: byte;
    TYPEF: byte;
    // Block1
    CIF: byte;  // 0x78
    //DIF1: byte; // 0x04
    //VIF1: byte; // 0x20 - time
    //CNT1: integer;    // time counter
    //DIF2: byte; // 0x01 1 byte
    //VIF2: word; // 0x17FD
    //ALARM: byte;    // alarmState
    //CRC1: word;
    BLOCK_N: array[0..PACKETMAX - 1 - (L_BLOCK0 - 2) - 1] of byte;
  end;
  PwmbPacket = ^TwmbPacket;

// WMBUS Fields
const
  wmbCMDlist: array[0..10] of TwmbCMD = (
    (CMDname: 'SND_NR'; CMDhex: $44; CMDhint: 'SEND/NO REPLY meter initiative'),
    (CMDname: 'SND_UD'; CMDhex: $53; CMDhint: 'SEND USER DATA'),
    (CMDname: 'SND_IR'; CMDhex: $46; CMDhint: 'Send manually initiated installation data'),
    (CMDname: 'SND_IR_ACK'; CMDhex: $06; CMDhint: 'ACK for INIT'),
    (CMDname: 'SND_AD'; CMDhex: $48; CMDhint: 'Accsess demand'),
    (CMDname: 'RSP_UD'; CMDhex: $08; CMDhint: 'Response of user data'),
    (CMDname: 'SND_END'; CMDhex: $40; CMDhint: 'END of LINK'),
    (CMDname: 'SND_ACK'; CMDhex: $00; CMDhint: 'ACK'),
    (CMDname: 'REQ_UD1'; CMDhex: $5A; CMDhint: 'REQUEST USER DATA1'),
    (CMDname: 'REQ_UD2'; CMDhex: $5B; CMDhint: 'REQUEST USER DATA2'),
    (CMDname: 'SND_ALARM'; CMDhex: $71; CMDhint: 'SEND Alarm')
    );
  // PACKET type
  SND_NR = $44;    // SEND/NO REPLY, meter initiative
  SND_UD = $53;    // SEND USER DATA
  SND_IR = $46;    // Send manually initiated installation data;
  SND_IR_ACK = $06;    // ACK for INIT;
  SND_AD = $48;    // Accsess demand
  RSP_UD = $08;    // response of user data
  SND_END = $40;    // END of LINK
  SND_ACK = $00;    // ACK
  REQ_UD1 = $5A;    // REQUEST USER DATA1
  REQ_UD2 = $5B;    // REQUEST USER DATA2
  // device unic number 8 digit BCD
  DEVMANID = 'KVD';
  DEVADR = '12345678';
  DEVTYPE = 7;
  DEVVER = 1;
  // Special DIF CODE
  // 2Fh Idle Filler (not to be interpreted), following byte = DIF of next record
  Idle_Filler = $2F;
  // 0Fh Start of manufacturer specific data structures to end of user data
  // 1Fh Same meaning as DIF = 0Fh + More records follow in next telegram

  L_PACKET40 = 25;
  L_PACKET53 = 25;
  L_PACKET48 = 12;


  oDIF1 = 13; // 0x04
  oVIF1 = 14; // 0x20 - time
  oCNT1 = 15;    // time counter
  oDIF2 = 19; // 0x01 1 byte
  oVIF2 = 20; // 0x17FD
  oALARM = 22;    // alarmState
  oCRC1 = 23;
  //************ CRC16 ECC Table **************
  //Poly = 0x3D65 Init = 0x0000
  ECCMAX = 18;
  ECCMIN = 3;
  wmbPoly = $3D65;
  wmbXORFF = $5C99;
  tableCRC: array [0..ECCMAX * 8 - 1] of word =
    ($3D65, $7ACA, $F594, $D64D, $91FF, $1E9B, $3D36, $7A6C,
    $F4D8, $D4D5, $94CF, $14FB, $29F6, $53EC, $A7D8, $72D5,
    $E5AA, $F631, $D107, $9F6B, $03B3, $0766, $0ECC, $1D98,
    $3B30, $7660, $ECC0, $E4E5, $F4AF, $D43B, $9513, $1743,
    $2E86, $5D0C, $BA18, $4955, $92AA, $1831, $3062, $60C4,
    $C188, $BE75, $418F, $831E, $3B59, $76B2, $ED64, $E7AD,
    $F23F, $D91B, $8F53, $23C3, $4786, $8F0C, $237D, $46FA,
    $8DF4, $268D, $4D1A, $9A34, $090D, $121A, $2434, $4868,
    $90D0, $1CC5, $398A, $7314, $E628, $F135, $DF0F, $837B,
    $3B93, $7726, $EE4C, $E1FD, $FE9F, $C05B, $BDD3, $46C3,
    $8D86, $2669, $4CD2, $99A4, $0E2D, $1C5A, $38B4, $7168,
    $E2D0, $F8C5, $CCEF, $A4BB, $7413, $E826, $ED29, $E737,
    $F30B, $DB73, $8B83, $2A63, $54C6, $A98C, $6E7D, $DCFA,
    $8491, $3447, $688E, $D11C, $9F5D, $03DF, $07BE, $0F7C,
    $1EF8, $3DF0, $7BE0, $F7C0, $D2E5, $98AF, $0C3B, $1876,
    $30EC, $61D8, $C3B0, $BA05, $496F, $92DE, $18D9, $31B2,
    $6364, $C6C8, $B0F5, $5C8F, $B91E, $4F59, $9EB2, $0001,
    $0002, $0004, $0008, $0010, $0020, $0040, $0080, $0100);

var
  pCnt: integer;



function wmbHexToBin(Value: string): integer;
function wmbBinToHex(Value: int64): string;
function wmbBcdToBin(Value: string): integer;
function wmbMAN_ID(Man: string): word;
function wmbMAN_ID_TO_STR(Id: word): string;
function wmbLFields(PacketLength: integer): integer;
function wmbPacketLength(LField: integer): integer;
function wmbECC(buf: PByte; cnt: integer): integer;
function wmbCheck(buf: PByte; cnt: integer): integer;
function wmbCRC(buf: PByte; cnt: integer): word;
function wmbCmpAdr(rxBuf: PByte; txBuf: PByte): boolean;

function wmbInit(buf: PByte): integer;

function wmbPrint(buf: PByte; cnt: integer): string;

function wmbAddCRC(buf: PByte): integer;

function wmbDelCRC(buf: PByte; cnt: integer): integer;

procedure wmbSetCI(buf: PwmbPacket; CI: byte);
procedure wmbAddTime(buf: PByte);

procedure wmbAddParam(buf: PByte; Dif: array of byte; Vif: array of byte; Value: int64);
procedure wmbAddAlarm(buf: PByte; alarm: byte);

procedure wmbCreate(buf: PByte; Cmd: byte);

procedure wmbDataIns(buf: PByte; Value: int64; cnt: integer);

procedure wmbProcess51(buf: PByte; cnt: integer; pPkt: PInteger);
procedure wmbProcess71(buf: PByte; cnt: integer; pPkt: PInteger);
procedure wmbProcess72(buf: PByte; cnt: integer; pPkt: PInteger);
procedure wmbProcess7A(buf: PByte; cnt: integer; pPkt: PInteger);
function wmbGetRecord(buf: PByte; cnt: integer; pPkt: PInteger; var sDif, sVif, sData: string): integer;


implementation

function IsHex(C: char): byte;
var
  h: byte;
begin
  if ((Ord(C) >= Ord('0')) and (Ord(C) <= Ord('9'))) then
    IsHex := Ord(C) - Ord('0')
  else if ((Ord(C) >= Ord('A')) and (Ord(C) <= Ord('F'))) then
    IsHex := Ord(C) - Ord('A') + 10
  else if ((Ord(C) >= Ord('a')) and (Ord(C) <= Ord('f'))) then
    IsHex := Ord(C) - Ord('a') + 10
  else
    IsHex := $FF;
end;

function Min(X, Y: integer): integer;
begin
  if X < Y then
     Result := X
  else
     Result := Y;
end;

function Max(X, Y: integer): integer;// stdcall;
begin
  if X > Y then
    Result := X
  else
     Result := Y;
end;

function wmbBinToHex(Value: int64): string;
var
  i, l: integer;
  str: string;
  d: byte;
begin
  str := '';
  repeat
    d := Value and $FF;
    str := str + IntToHex(d, 2);
    Value := Value shr 8;
  until Value = 0;
  //l := Length(str);
  //for i := 1 to (Length(str) div 2) do
  //begin
  //  strR := strR + str[l - 1];
  //  strR := strR + str[l];
  //  l := l - 2;
  //end;
  Result := str;
end;

function wmbHexToBin(Value: string): integer;
var
  i, c: integer;
begin
  Result := 0;
  for i := 1 to Length(Value) do
  begin
    c := IsHex(Value[i]);
    if c > 15 then
      Exit;
    Result := Result * 16 + c;
  end;
end;

function wmbBcdToBin(Value: string): integer;
var
  i, c: integer;
begin
  Result := 0;
  for i := 1 to Length(Value) do
  begin
    c := IsHex(Value[i]);
    if c > 9 then
      Exit;
    Result := Result * 16 + c;
  end;
end;

function wmbPrint(buf: PByte; cnt: integer): string;
var
  str: string;
  i: integer;
begin
  str := '';
  for i := 0 to cnt - 1 do
    str := str + Dec2Numb(buf[i], 2, 16);
  Result := str;
end;

function wmbGetRecord(buf: PByte; cnt: integer; pPkt: PInteger; var sDif, sVif, sData: string): integer;
var
  i: integer;
  wcnt, m: int64;
  dif: integer;
  d: byte;
  sec, min, hour, dow, day, month, year: integer;
begin
  Result := 0;
  sDif := '';
  sVif := '';
  sData := '';
  // DIF
  dif := 0;
  repeat
    d := (buf + pPkt^)^;
    Inc(pPkt^);
    dif := d;
    sDif := sDif + IntToHex(d, 2);
  until ((d and $80) = 0) or (pPkt^ >= cnt);
  dif := dif and $07;
  // Length data
  if dif = 5 then
    dif := 4;
  if dif = 7 then
    dif := 8;
  // VIF
  repeat
    d := (buf + pPkt^)^;
    Inc(pPkt^);
    sVif := sVif + IntToHex(d, 2);
  until ((d and $80) = 0) or (pPkt^ >= cnt);
  // DATA
  wcnt := 0;
  m := 1;
  while ((dif > 0) and (pPkt^ < cnt)) do
  begin
    d := (buf + pPkt^)^;
    Inc(pPkt^);
    Dec(dif);
    wcnt := wcnt + m * d;
    m := m shl 8;
  end;
  // ret data
  sDif := sDif + 'h';
  sVif := sVif + 'h';
  if sVif = '6Dh' then
  begin
    if sDif = '07h' then
    begin

      sData := Format('%.2u', [(wcnt shr 16) and $FF]) + ':';
      sData := sData + Format('%.2u', [(wcnt shr 8) and $FF]) + ':';
      sData := sData + Format('%.2u', [(wcnt shr 0) and $FF]) + ' ';
      sData := sData + IntToStr((wcnt shr 24) and $FF) + ' ';
      sData := sData + IntToStr((wcnt shr 32) and $FF) + '-';
      sData := sData + IntToStr((wcnt shr 40) and $FF) + '-';
      sData := sData + IntToStr((wcnt shr 48) and $FFFF);
    end
    else if sDif = '04h' then
    begin

      hour := wcnt div 3600;
      sec := wcnt mod 3600;
      min := sec div 60;
      sec := sec mod 60;

      sData := IntToStr(hour) + ':';
      sData := sData + IntToStr(min) + ':';
      sData := sData + IntToStr(sec);

    end;

  end
  else
    sData := IntToStr(wcnt);
  Result := 1;
end;


procedure wmbProcess51(buf: PByte; cnt: integer; pPkt: PInteger);
var
  p: PByte;
  // Send User Data
begin
  pPkt^ := pPkt^ + 0;
end;

procedure wmbProcess71(buf: PByte; cnt: integer; pPkt: PInteger);
var
  p: PByte;
  // Alarm
begin
  pPkt^ := pPkt^ + 0;
end;

procedure wmbProcess72(buf: PByte; cnt: integer; pPkt: PInteger);
var
  p: PByte;
  // Decrypt Message full header
begin
  pPkt^ := pPkt^ + 12;
end;

procedure wmbProcess7A(buf: PByte; cnt: integer; pPkt: PInteger);
var
  p: PByte;
  // Decrypt Message   short header
begin
  pPkt^ := pPkt^ + 4;

end;



procedure wmbCreate(buf: PByte; Cmd: byte);
{
В существующий пакет с адресом записывает новые данные
возвращает длину пакета
}
begin
  buf^ := L_BLOCK0 - 3;
  (buf + oCF)^ := Cmd;
end;

//ATF=145300001111111103A4840B7804140180000001FD1700AC3B
//ATF=1453C42E78563412010798A77804140180000001FD1700AC3B
//ATF=140800001111111103A4422D7804140180000001FD1700AC3B
procedure wmbAddTime(buf: PByte);
// Добавляет Time поле в конец пакета
var
  i: integer;
  str: string;
  wmbCurrentTime: TDateTime;
  //  pCurrentTime: PDateTime;
  pCurrentTime: PInt64;
  pB: PByte;
  W: int64;
  pDIF, pVIF: array[0..9] of byte;
  //function Now: TDateTime;
  // Функция возвращает текущую дату и время
begin
  // DIF   // VIF   // DATA
  wmbCurrentTime := Now;
  pCurrentTime := @wmbCurrentTime;
  pB := @W;
  pDIF[0] := $07;
  pDIF[1] := 0;
  pVIF[0] := $6D;
  pVIF[1] := 0;
  pB^ := StrToInt(FormatDateTime('ss', pCurrentTime^));
  Inc(pB);
  pB^ := StrToInt(FormatDateTime('nn', pCurrentTime^));
  Inc(pB);
  pB^ := StrToInt(FormatDateTime('hh', pCurrentTime^));
  Inc(pB);
  pB^ := DayOfWeek(pCurrentTime^) - 1;
  Inc(pB);
  pB^ := StrToInt(FormatDateTime('DD', pCurrentTime^));
  Inc(pB);
  pB^ := StrToInt(FormatDateTime('MM', pCurrentTime^));
  Inc(pB);
  pB^ := StrToInt(FormatDateTime('YYYY', pCurrentTime^)) mod 256;
  Inc(pB);
  pB^ := StrToInt(FormatDateTime('ss', pCurrentTime^)) div 256;
  wmbAddParam(buf, pDIF, pVIF, W);
end;

procedure wmbAddAlarm(buf: PByte; alarm: byte);
// Добавляет Alarm поле в конец пакета
var
  pDIF, pVIF: array[0..9] of byte;
begin
  pDIF[0] := $01;
  pDIF[1] := 0;
  pVIF[0] := $FD;
  pVIF[1] := $17;
  pVIF[2] := 0;
  wmbAddParam(buf, pDIF, pVIF, alarm);
  //wmbAddParam(buf, $01, $17FD, alarm);
end;


function wmbDelCRC(buf: PByte; cnt: integer): integer;
  // Удаляет  CRC16
var
  i, p, total, L: integer;
  wrk: array[0..PACKETMAX - 1] of byte;
begin
  p := 0;
  total := 0;
  // Block 0
  L := L_BLOCK0 - 2;
  for i := 0 to L - 1 do
    wrk[total + i] := (buf + p + i)^;
  total := total + L;
  p := p + L + 2;
  //  Block 1 .. N
  while ((cnt - p) > 2) do
  begin
    if ((cnt - p) > L_BLOCK1) then
      L := L_BLOCK1 - 2
    else
      L := (cnt - p - 2);
    for i := 0 to L - 1 do
      wrk[total + i] := (buf + p + i)^;
    total := total + L;
    p := p + L + 2;
  end;
  // копирование результата
  for i := 0 to total - 1 do
    (buf + i)^ := wrk[i];
  Result := total;
end;

function wmbAddCRC(buf: PByte): integer;
  // Разделяет пакет на блоки и добавляет CRC16
var
  i, p, total, L: integer;
  crc16: word;
  wrk: array[0..PACKETMAX - 1] of byte;
begin
  if (buf^ < wmbLFields(L_BLOCK0)) then
  begin
    Result := 0;
    Exit;
  end;
  if (buf^ > wmbLFields(PACKETMAX)) then
  begin
    Result := 0;
    Exit;
  end;
  p := 0;
  total := 0;
  // Block 0
  L := L_BLOCK0 - 2;
  for i := 0 to L - 1 do
    wrk[total + i] := (buf + p + i)^;
  crc16 := wmbCRC(buf + p, L);
  wrk[total + L] := (crc16 shr 8) and $FF;
  ;
  wrk[total + L + 1] := crc16 and $FF;
  total := total + L + 2;
  p := p + L;
  //  Block 1 .. N
  while ((buf^ + 1 - p) > 0) do
  begin
    if ((buf^ + 1 - p) > (L_BLOCK1 - 2)) then
      L := L_BLOCK1 - 2
    else
      L := (buf^ + 1 - p);
    for i := 0 to L - 1 do
      wrk[total + i] := (buf + p + i)^;
    crc16 := wmbCRC(buf + p, L);
    wrk[total + L] := (crc16 shr 8) and $FF;
    ;
    wrk[total + L + 1] := crc16 and $FF;
    total := total + L + 2;
    p := p + L;
  end;
  // копирование результата
  for i := 0 to total - 1 do
    (buf + i)^ := wrk[i];
  Result := total;
end;

function wmbInit(buf: PByte): integer;
  // Инициализация адреса из флэш-памяти (файла)
var
  i: integer;
  c: byte;
  mId, crc16: word;
begin
  (buf + oLF)^ := wmbLFields(L_BLOCK0);  //L_PACKET53);//
  (buf + oCF)^ := SND_UD;
  mId := wmbMAN_ID(DEVMANID);
  (buf + oMANIDF)^ := mId mod 256;
  (buf + oMANIDF + 1)^ := mId div 256;
  //DEVADR
  for i := 1 to 4 do
  begin
    c := IsHex(DEVADR[10 - 2 * i]);
    c := c + IsHex(DEVADR[9 - 2 * i]) * 16;
    (buf + oADDRF + i - 1)^ := c;
  end;
  (buf + oVERF)^ := DEVVER;
  (buf + oTYPEF)^ := DEVTYPE;
  //wmbAddCI(buf, $7A);
  //wmbAddParam(buf, $07);
  //wmbAddAlarm(buf, $77);
  //wmbAddParam(buf, $00);
  //wmbAddParam(buf, $07);
  //wmbAddAlarm(buf, $77);
  //wmbAddParam(buf, $00);
  //wmbAddParam(buf, $07);
  //wmbAddAlarm(buf, $77);
  //wmbAddParam(buf, $00);
  Result := wmbAddCRC(buf);
end;

function wmbCmpAdr(rxBuf: PByte; txBuf: PByte): boolean;
  // Сравнение адресов устройств двух пакетов
var
  i: integer;
begin
  Result := True;
  for i := oMANIDF to oTYPEF do
  begin
    if ((rxBuf + i)^ <> (txBuf + i)^) then
    begin
      Result := False;
      //Exit;
    end;
  end;
end;

function wmbCheck(buf: PByte; cnt: integer): integer;
  // Проверка целостности пакета по CRC16
  // Исправляется 2 ошибочных бита в блоке
  // Ограничение на длину пакета
  // Возвращает количество правильных байтов
var
  packetL, total, p: integer;
begin
  if (cnt < L_BLOCK0) then    // RADIO
  begin
    Result := 0;
    Exit;
  end;
  if (cnt > PACKETMAX) then
  begin
    Result := 0;
    Exit;
  end;
  // Block 0
  if (wmbECC(buf, L_BLOCK0) > 2) then     // RADIO
  begin
    Result := 0;
    Exit;
  end;
  packetL := wmbPacketLength(buf^);
  if (packetL > cnt) then
  begin
    packetL := cnt;
    //Result := 0;
    //Exit;
  end;
  if (packetL < L_BLOCK0) then      // RADIO
  begin
    Result := 0;
    Exit;
  end;
  if (packetL > PACKETMAX) then
  begin
    Result := 0;
    Exit;
  end;
  //  Block 1 .. N
  total := L_BLOCK0;
  while (total < packetL) do
  begin
    p := packetL - total;
    if (p > L_BLOCK1) then
      p := L_BLOCK1;
    if (wmbECC(buf + total, p) > 2) then    // RADIO
    begin
      Result := total;
      Exit;
    end
    else
      total := total + p;
  end;
  Result := total;
end;

function wmbECC(buf: PByte; cnt: integer): integer;
  // Проверка целостности блока по CRC16
  // Исправляется 2 ошибочных бита в блоке
  // Возвращает количество ошибок
var
  iByte, iBit, pos1, pos2: integer;
  crc16: word;
begin
  if ((cnt > ECCMAX) or (cnt < ECCMIN)) then
  begin
    Result := 0;
    Exit;
  end;
  crc16 := wmbCRC(buf, cnt) xor wmbXORFF;
  // No Errors
  if (crc16 = 0) then
  begin
    Result := 0;
    Exit;
  end;
  // One Error
  for pos1 := 0 to cnt * 8 - 1 do
  begin
    if (crc16 = tableCRC[pos1]) then
    begin
      iByte := cnt - 1 - (pos1 div 8);
      iBit := pos1 mod 8;
      (buf + iByte)^ := (buf + iByte)^ xor (1 shl iBit);
      Result := 1;
      Exit;
    end;
  end;
  //Two Errors
  for pos1 := 0 to cnt * 8 - 1 do
  begin
    crc16 := crc16 xor tableCRC[pos1];
    for pos2 := 0 to cnt * 8 - 1 do
    begin
      if (crc16 = tableCRC[pos2]) then
      begin
        // First Bit
        iByte := cnt - 1 - (pos1 div 8);
        iBit := pos1 mod 8;
        (buf +iByte)^ := (buf + iByte)^ xor (1 shl iBit);
        // Second Bit
        iByte := cnt - 1 - (pos2 div 8);
        iBit := pos2 mod 8;
        (buf +iByte)^ := (buf + iByte)^ xor (1 shl iBit);
        Result := 2;
        Exit;
      end;
    end;
    crc16 := crc16 xor tableCRC[pos1];
  end;
  // Many Errors
  Result := 3;
end;

function wmbCRC(buf: PByte; cnt: integer): word;
  // CRC: Cyclic redundancy check
  // The CRC polynomial is: x16 + x13 + x12 + x11 + x10 + x8 + x6 + x5 + x2 + 1
  // The initial value is:0
  // The final CRC is complemented
var
  i: integer;
  crc16: word;
begin
  crc16 := 0;
  while (cnt > 0) do
  begin
    crc16 := crc16 xor (buf^ shl 8);
    Dec(cnt);
    Inc(buf);
    for i := 1 to 8 do
    begin
      if ((crc16 and $8000) = $8000) then
      begin
        crc16 := (crc16 shl 1) and $FFFF;
        crc16 := crc16 xor wmbPoly;
      end
      else
        crc16 := (crc16 shl 1) and $FFFF;
    end;
  end;
  Result := (crc16 xor $FFFF) and $FFFF;
  //Result := crc16 xor wmbXOR;
end;
// Manufacturer ID
//#define MAN_ID(X,Y,Z) ((X-64)*32*32+(Y-64)*32+(Z-64))
//#define BETAR_ID    MAN_ID('B','T','R')
//#define KVADRAT_ID  MAN_ID('K','V','D')
function wmbMAN_ID_TO_STR(Id: word): string;
var
  Man: string;
  i: word;
begin
  i := Id div (32 * 32);
  Man := char(i + 64);
  Id := Id - (i * 32 * 32);
  i := Id div 32;
  Man := Man + char(i + 64);
  Id := Id - (i * 32);
  i := Id div 1;
  Man := Man + char(i + 64);
  Result := Man;
end;

function wmbMAN_ID(Man: string): word;
begin
  Result := ((Ord(Man[1]) - 64) * 32 * 32 + (Ord(Man[2]) - 64) * 32 + (Ord(Man[3]) - 64));


end;

function wmbPacketLength(LField: integer): integer;
begin
  Result := ((LField + 3) + 2 * ((LField + 6) div 16));
end;

function wmbLFields(PacketLength: integer): integer;
begin
  Result := (PacketLength - (3 + 2 * ((PacketLength + 5) div 18)));
end;

procedure wmbDataIns(buf: PByte; Value: int64; cnt: integer);
begin
  while (cnt > 0) do
  begin
    Inc(buf^);
    (buf +buf^)^ := Value and $FF;
    Value := Value shr 8;
    Dec(cnt);
  end;
end;

procedure wmbAddParam(buf: PByte; Dif: array of byte; Vif: array of byte; Value: int64);
var
  i, length: integer;
begin
  length := 0;
  i := 0;
  // DIF
  repeat
    Inc(buf^);
    (buf + buf^)^ := Dif[i] and $FF;
    length := Dif[i] and $FF;
    //Dif := Dif shr 8;
    Inc(i);
  until Dif[i] = 0;
  length := length and $07;
  if length = 5 then
    length := 4;
  if length = 7 then
    length := 8;
  // VIF
  i := 0;
  repeat
    Inc(buf^);
    (buf + buf^)^ := Vif[i] and $FF;
    //Vif := Vif shr 8;
    Inc(i);
  until Vif[i] = 0;
  // Data
  while length > 0 do
  begin
    Inc(buf^);
    (buf + buf^)^ := Value and $FF;
    Dec(length);
    Value := Value shr 8;
  end;
end;
////
procedure wmbSetCI(buf: PwmbPacket; CI: byte);
// Добавляет CI поле в конец пакета
var
  i: integer;
begin
  buf^.LF := L_BLOCK0 - 2;
  buf^.CIF := CI;
  //buf^ := L_BLOCK0 - 2;
  //(buf + buf^)^ := CI;
  case CI of
    $51:
    begin
      //i := 1;
    end;
    $78:
    begin
      //i := 1;
    end;
    $72:
    {
       Ident. Nr. Manufr. Version Device type Access No. Status Signature
             4 Byte 2 Byte 1 Byte 1 Byte 1 Byte 1 Byte 2 Byte
             The address fields always contain the address of the meter
       independent of the Transmission direction.
             In case of wireless communication based on EN 13757-4, this
       CI-Field shall be used when the Link
             layer address differs from meter address.
      }
    begin
      //buf^ := buf^ + 12;
      //for i := 0 to 12 do
      //begin
      //  Inc(buf^);
      //  (buf +buf^)^ := 0;
      //end;
      //i := 0;
      //wmbDataIns(buf, @i, 4);  // Ident. Nr.
      //wmbDataIns(buf, @i, 2);  // Manufr.
      //wmbDataIns(buf, @i, 1);  // Version
      //wmbDataIns(buf, @i, 1);  // Device type
      //wmbDataIns(buf, @i, 1);  // Access No.
      //wmbDataIns(buf, @i, 1);  // Status
      //wmbDataIns(buf, @i, 2);  // Signature
      {
      2 DES encryption with CBC; Initialization vector is zero (deprecated)
      3 DES encryption with CBC; Initialization vector is not zero (deprecated)
      5 AES encryption with CBC; Initialization vector is not zero
      Signature is a 2 bytes word containing the length of encrypted content
      (in the first byte)
            and the encryption method code (in the 4 least significant bits of
      the second byte). Since
            encryption and decryption can only be performed in
            blocks, the number of encrypted
            bytes is a multiple of the block size (8 for DES and 16 for
      AES-128). Therefore, the 3 or
            4 least significant bits of the first byte of the signature word
      do not enter in the count of
            encrypted bytes, and can be used for other purposes.
            The supported encryption methods are identified by
            codes 2, 3, 4 and 5. Methods 2 and 3 use
            DES encryption, while methods 4 and 5 use AES-128.
            Methods 3 and 5 need the long data
            header (12 bytes) to initialize the CBC algorithm,
            therefore they can be used only with CI
            field values 0x72 and 0x5B; methods 2 and 4 can be
            used with any of the four CI-Field
            values. Method 3 needs the current date to initialize the CBC
      algorithm, therefore in order to
            communicate with this encryption method a meter must have the same
      date as set in the
      concentrator.
      }
      //      Result := 12 + 1;
    end;
    $7A:
    begin
      {
      Inc(buf^); Access No.:1 Status:1 Signature:2
The Access Number has unsigned binary coding, and is incremented (modulo 256)
by one before or after each RSP_UD from the slave. Since it can also be used
to enable private end users to detect an unwanted overfrequently readout of
its consumption meters, it should not be resettable by any bus communication.

b) The initialization vector for encryption modes 5 is
(written in low to high order according to the
AES standard FIPS 197):
Table 14 — Initialization vector mode 5 for the CBC-AES-128
LSB 1 2 3 4 5 6 7 8 9 10 11 12 13 14 MSB
Manuf.
LSB .. MSB ID
Version
Device type
LSB .. MSB Acc. no.
The Initialization vector always contains the address of the meter.

      }
      //for i:=0 to 1 do begin
      //Inc(buf^);
      //(buf +buf^)^ := 0;
            {
               Coding of the Status Field
             0 0 No Error
             2 Power low Not power low
             3 Permanent error
             4 Temporary error
             5 Specific to manufacturer Specific to manufacturer
            }
      //for i:=0 to 1 do begin
      //Inc(buf^);
      //(buf +buf^)^ := 0;
            {
The Signature is reserved for optional encryption of the application data.
Such an encryption might be required for transmit only wireless meter readout.
It is assumed, that each meter (or a group of meters) could have an individual
encryption key. If no Encryption is used its value shall be 00 00 h.
      }
      //for i:=0 to 2 do begin
      //Inc(buf^);
      //(buf +buf^)^ := 0;
      {
       5.10.3 Structure of encrypted telegrams
       a) The data header (CI=72h see 5.2 or CI = 7Ah see 5.3) is always
       unencrypted. The last word of this block
       is the signature word. If the following data are unencrypted, this
       signature word contains a zero.
       b) If the transmission contains encrypted data, the high byte of this
       signature word contains a code for the
       encryption method. The code 0 signals no encryption. Currently only the
       encryption codes 02xxh or 03xxh
       (see below) are defined. The other codes are reserved. The number of
       encrypted bytes is contained in
       the low byte of the signature word. The content of this signature word
       had been defined in the EN 1434–3
       as zero, corresponding consistently to no encrypted data.
       c) The encrypted data follow directly after the signature word, thus
       forming the beginning of the DIF/VIF-
       structured part of the telegram.
       5.10.4 Partial Encryption
       a) If the number of encrypted bytes is less than the remaining data of
       the telegram, unencrypted data may
       follow after the encrypted data. They shall start at a record boundary,
       i.e. the first byte after the encrypted
       data will be interpreted as a DIF.
       b) If a partially encrypted telegram shall contain encrypted
       manufacturer specific data a record with a
       suitable length DIF (possibly a variable length string DIF) and a VIF =
       7Fh (manufacturer specific data
       record) shall be used instead of the usual MDH-DIF = 0Fh. This is
       required to enable after decryption
       standard DIF/VIF-decoding of a previously partially encrypted telegram
       containing encrypted
       manufacturer specific data.
       5.10.5 Encryption methods
       a) Encryption according to the DES (data encryption standard) as
       described in ANSI X3.92:1981;
       b) Cipher Block Chaining (CBC)-method as described in ANSI X3.106:1983
       with an initial initialization vector
       of zero: (Encryption Method Code = 02xxh). In this case the data
       records should contain the current date
       before the meter reading.
       Note that in this case the data after the date record, i.e. especially
       the encrypted meter reading data
       change once per day even if their data content itself is constant. This
       prevents an undetectable later
       playback of stored encrypted meter readings by a hacker.
       c)
       The "Initialization Vector IV" with length 64 bits of this standard may
       alternatively be defined by the first
       6 bytes of the identification header in mode 1 sequence, i.e.
       identification number in the lowest 4 bytes
       followed by the manufacturer ID in the two next higher bytes and
       finally by the current date coded as in
       record structure "G" for the two highest bytes.
       In this case the Encryption method is coded as "03xxh". Note that in
       this case all encrypted data change
       once per day even if the data content itself is constant. This prevents
       an undetectable later playback of
       any stored encrypted data by a hacker.
       d) To simplify the verification of correct decoding and to prevent an
       undetected change in the identification
       of the not encrypted header, the encrypted part of the telegram shall
       contain at least together with the
       appropriate application layer coding (DIF and VIF) again the same
       identification number as in the
       unencrypted header;
       e) Due to the mathematical nature of the DES-algorithm the encrypted
       length contained in the low byte of
       the signature word shall be an integer multiple of 8 if the high byte
       signals DES-Encryption. Unused bytes
       in the last 8-byte block shall be filled with appropriatly structured
       dummy data records to achieve the
       required record boundary at the end of the encrypted data. One or
       several bytes containing the filler
       DIF = 2Fh are suggested to fill such gaps;
       f) The application of certain Encryption methods might be prohibited by
       local law
      }
      //      Result := 4 + 1;
    end;

    else
    begin
      buf^.LF := L_BLOCK0 - 3;
      //      Result := 0;
    end;
  end;
end;

end.