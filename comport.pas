unit comport;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Process, FileUtil, Forms, Controls, Graphics,
  Dialogs, StdCtrls, SdpoSerial;

type

  { TForm2 }

  TForm2 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    ComboBox3: TComboBox;
    ComboBox4: TComboBox;
    ComboBox5: TComboBox;
    ComboBox6: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Process1: TProcess;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Form2Show(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.lfm}

{ TForm2 }

procedure TForm2.Button1Click(Sender: TObject);
begin
  //Close;
  //Form2.ModalResult := mrOk;
end;

procedure TForm2.Button2Click(Sender: TObject);
begin
  //Close;
  //Form2.ModalResult := mrCancel;
end;

procedure TForm2.ComboBox1Change(Sender: TObject);
begin

end;

procedure TForm2.FormCreate(Sender: TObject);
var
  br: TBaudRate;
  db: TDataBits;
  pr: TParity;
  sb: TStopBits;
  fc: TFlowControl;
  str: string;
begin
  //Form2.ComboBox1.Items.Add('/dev/ttyUSB0');
  //Form2.ComboBox1.ItemIndex := 0;
  for br in TBaudRate do
    Form2.ComboBox2.Items.Add(IntToStr(ConstsBaud[br]));
  Form2.ComboBox2.ItemIndex := 9;
  for db in TDataBits do
    Form2.ComboBox3.Items.Add(IntToStr(ConstsBits[db]));
  Form2.ComboBox3.ItemIndex := 0;
  for pr in TParity do
    Form2.ComboBox4.Items.Add(ConstsParity[pr]);
  Form2.ComboBox4.ItemIndex := 0;
  for fc in TFlowControl do
  begin
    WriteStr(str, fc);
    Form2.ComboBox5.Items.Add(str);
  end;
  Form2.ComboBox5.ItemIndex := 0;
  for sb in TStopBits do
  begin
    WriteStr(str, sb);
    Form2.ComboBox6.Items.Add(str);
    //Form2.ComboBox6.Items.Add(IntToStr(ConstsStopBits[sb]));
  end;
  Form2.ComboBox6.ItemIndex := 0;

end;

{$IFDEF MSWINDOWS}
//{$IFDEF MSWINDOWS}
procedure GetSerialPortList(list: TStrings);
var
  reg: TRegistry;
  l, tmp: TStringList;
  n: integer;
begin
  tmp := TStringList.Create;
  l := TStringList.Create;
  reg := TRegistry.Create;
  try
    reg.Access := KEY_READ;
    reg.RootKey := HKEY_LOCAL_MACHINE;
    reg.OpenKey('\HARDWARE\DEVICEMAP\SERIALCOMM', False);
    reg.GetValueNames(l);
    for n := 0 to l.Count - 1 do
    begin
      tmp.Add(reg.ReadString(l[n]));
    end;

    tmp.Sort;
    list.Assign(tmp);
  finally
    reg.Free;
    l.Free;
    tmp.Free;
  end;
end;
//{$ELSE}

{$ENDIF}

{$IFNDEF MSWINDOWS}

procedure TForm2.Form2Show(Sender: TObject);
var
  //Dmesg: TStringList; j, p,
  u: integer;
  uart: string;
  uartFile: file;
begin
  //Dmesg := TStringList.Create;
  // dmesg
  //Process1.Create(nil);
  // On Linux/Unix/OSX, we need specify full path to our executable:
  //hProcess.Executable := '/bin/sh';
  //// Now we add all the parameters on the command line:
  //hprocess.Parameters.Add('-c');
  //// Here we pipe the password to the sudo command which then executes fdisk -l:
  //hprocess.Parameters.add('echo ' + sPass  + ' | sudo -S fdisk -l');
  //// Run asynchronously (wait for process to exit) and use pipes so we can read the output pipe
  //hProcess.Options := hProcess.Options + [poWaitOnExit, poUsePipes];
  //// Now run:
  //hProcess.Execute;



  //Process1.Executable := 'dmesg';
  // Process1.CommandLine := '/bin/ls /dev/tty*';
  //Process1.Executable := '/bin/sh';
  //  Process1.Parameters.Add('-c');
  //Process1.Parameters.Add('echo ' + ' | ls /dev/ttyU*');
  //Process1.Options := Process1.Options + [poWaitOnExit, poUsePipes];
  //Process1.Execute;
  //Dmesg.LoadFromStream(Process1.Output);
  //Process1.Free;
  ComboBox1.Items.Clear;
  //  | grep tty
  //for j := 0 to Dmesg.Count - 1 do
  //begin
  //  p := Pos('tty', Dmesg.Strings[j]);
  //  if p > 0 then
  //  begin
  // ttyS
  for u := 0 to 9 do
    //begin
    //  p := Pos('ttyS' + IntToStr(u), Dmesg.Strings[j]);
    //  if p > 0 then
  begin
    uart := '/dev/ttyUSB' + IntToStr(u);
    if (FileExists(uart)) then
      ComboBox1.Items.Add(uart);
    uart := '/dev/ttyACM' + IntToStr(u);
    if (FileExists(uart)) then
      ComboBox1.Items.Add(uart);
    uart := '/dev/ttyS' + IntToStr(u);
    if (FileExists(uart)) then
      ComboBox1.Items.Add(uart);
    //ReceiveMemo1.Lines.Add(uart);
  end;
  //end;
  //// ttyACM
  //for u := 0 to 9 do
  //begin
  //  //p := Pos('ttyACM' + IntToStr(u), Dmesg.Strings[j]);
  //  //if p > 0 then
  //  begin
  //    uart := '/dev/ttyACM' + IntToStr(u);
  //    ComboBox1.Items.Add(uart);
  //    //ReceiveMemo1.Lines.Add(uart);
  //  end;
  ////end;
  //// ttyUSB
  //for u := 0 to 9 do
  ////begin
  ////  p := Pos('ttyUSB' + IntToStr(u), Dmesg.Strings[j]);
  ////  if p > 0 then
  //  begin
  //    uart := '/dev/ttyUSB' + IntToStr(u);
  //    ComboBox1.Items.Add(uart);
  //    //ReceiveMemo1.Lines.Add(uart);
  //  end;
  //end;
  //ReceiveMemo1.Lines.Add(Dmesg.Strings[j]);
  //end;
  //end;
  //Dmesg.Free;

end;


{$ENDIF}

//procedure TnaykSerial.SaveSettingsToFile(const FileName: String);
//begin
//  with TIniFile.Create(FileName) do
//  try
//    WriteString('PortSettings', 'Device', FDevice);
//    WriteString('PortSettings', 'BaudRate', BaudRateToStr( FBaudRate ));
//    WriteString('PortSettings', 'DataBits', DataBitsToStr( FDataBits ));
//    WriteString('PortSettings', 'StopBits', StopBitsToStr( FStopBits ));
//    WriteString('PortSettings', 'Parity', ParityToStr( FParity ));
//    WriteString('PortSettings', 'FlowControl', FlowControlToStr( FFlowControl ));
//  finally
//    Free;
//  end;
//end;

//procedure TnaykSerial.LoadSettingsFromFile(const FileName: string);
//begin
//  with TIniFile.Create(FileName) do
//  try
//    FDevice:=ReadString('PortSettings', 'Device', FDefDevice);
//    FBaudRate:=StrToBaudRate(ReadString('PortSettings', 'BaudRate', '9600'));
//    FDataBits:=StrToDataBits(ReadString('PortSettings', 'DataBits', '8'));
//    FStopBits:=StrToStopBits(ReadString('PortSettings', 'StopBits', '1'));
//    FParity:=StrToParity(ReadString('PortSettings', 'Parity', 'None'));
//    FFlowControl:=StrToFlowControl(ReadString('PortSettings', 'FlowControl', 'None'));
//  finally
//    Free;
//  end;
//end;

//function TnaykSerial.GetPortsList(List: TStrings): integer;
//var tmp: TStringList;
//    i, n: integer;
//begin
//  tmp:=TStringList.Create;
//  result := -1;
//  try
//    GetSerialPortList(tmp);
//    List.Assign(tmp);

//    n := tmp.Count-1;
//    for i:=0 to n do
//    begin
//      if (tmp.Strings[i] = FDevice) then
//      begin
//        result:=i;
//        break;
//      end;
//    end;
//  finally
//    tmp.Free;
//  end;

//{Функции} //============================================================================
//{$IFDEF MSWINDOWS}
//procedure GetSerialPortList(list: TStrings);
//var
//  reg: TRegistry;
//  l, tmp: TStringList;
//  n: integer;
//  str: string;
//begin
//  tmp := TStringList.Create;
//  l := TStringList.Create;
//  reg := TRegistry.Create;
//  try
//    reg.Access := KEY_READ;
//    reg.RootKey := HKEY_LOCAL_MACHINE;
//    reg.OpenKey('\HARDWARE\DEVICEMAP\SERIALCOMM', false);
//    reg.GetValueNames(l);
//    for n := 0 to l.Count - 1 do
//    begin
//      str:=reg.ReadString(l[n]);
//      if PortAvailable(str) then
//      tmp.Add(str);
//    end;

//    list.Assign(tmp);
//  finally
//    reg.Free;
//    l.Free;
//    tmp.Free;
//  end;
//end;
//{$ENDIF}

//{$IFNDEF MSWINDOWS}
//procedure GetSerialPortList(list: TStrings);
//var
//  sr : TSearchRec;
//  tmp: TStringList;
//begin
//  tmp:=TStringList.Create;
//  try

//    if FindFirst('/dev/ttyS*', $7FFFFFFF, sr) = 0 then
//    begin
//      repeat
//        if (sr.Attr and $7FFFFFFF) = Sr.Attr then
//        begin
//          if PortAvailable( '/dev/' + sr.Name ) then
//            tmp.Add('/dev/' + sr.Name);
//        end;
//      until FindNext(sr) <> 0;
//    end;
//    FindClose(sr);

//    if FindFirst('/dev/ttyACM*', $7FFFFFFF, sr) = 0 then
//    begin
//      repeat
//        if (sr.Attr and $7FFFFFFF) = Sr.Attr then
//        begin
//          if PortAvailable( '/dev/' + sr.Name ) then
//            tmp.Add('/dev/' + sr.Name);
//        end;
//      until FindNext(sr) <> 0;
//    end;
//    FindClose(sr);

//    if FindFirst('/dev/ttyUSB*', $7FFFFFFF, sr) = 0 then
//    begin
//      repeat
//        if (sr.Attr and $7FFFFFFF) = Sr.Attr then
//        begin
//          if PortAvailable( '/dev/' + sr.Name ) then
//            tmp.Add('/dev/' + sr.Name);
//        end;
//      until FindNext(sr) <> 0;
//    end;
//    FindClose(sr);

//    if FindFirst('/dev/com/*', $7FFFFFFF, sr) = 0 then
//    begin
//      repeat
//        if (sr.Attr and $7FFFFFFF) = Sr.Attr then
//        begin
//          if PortAvailable( '/dev/com/' + sr.Name ) then
//            tmp.Add('/dev/com/' + sr.Name);
//        end;
//      until FindNext(sr) <> 0;
//    end;
//    FindClose(sr);


//    list.Assign(tmp);
//  finally
//    tmp.Free;
//  end;
//end;
//{$ENDIF}


//end;


end.
