program UsbACM0;

{$IFDEF LINUX}
  {$IFNDEF UseCThreads}
    {$DEFINE UseCThreads}
  {$ENDIF}
{$ENDIF}

{$mode objfpc}{$H+}

uses {$IFDEF UseCThreads}
  cthreads, {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms,
  UsbFlasher,
  wmbus,
  naykSerial;

{$R *.res}

begin
  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
