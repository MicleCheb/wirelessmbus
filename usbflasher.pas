unit UsbFlasher;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, process, FileUtil, Forms, Controls, Graphics,
  Dialogs, Menus, ExtCtrls, StdCtrls, Buttons, LCLType, ComCtrls, IniFiles,
  Grids, types, LCLProc,
  // Модуль СОМ порта
  naykSerial,
  wmbus; //strutils,

const
  sAppTitle = 'Шаблон приложения для работы с COM-портом';
  sAuthor = 'Тетерин Евгений';
  sVer = '1.0 beta (2014)';
  sMail = 'nayk@nxt.ru';
  sIcq = '262088737';

  sSettingsDir = 'SerialPortTemplate';

type
  { TForm1 }

  TForm1 = class(TForm)
    AsciiBox1: TComboBox;
    AddButton: TButton;
    AlDIF: TEdit;
    AlVIF: TEdit;
    AlValue: TEdit;
    NewButton: TButton;
    PKTButton: TButton;
    TimeCheckBox: TCheckBox;
    CmdComboBox: TComboBox;
    MANIDEdit: TEdit;
    ADREdit: TEdit;
    VEREdit: TEdit;
    TYPEEdit: TEdit;
    CIEdit: TEdit;
    DIFEdit: TEdit;
    VIFEdit: TEdit;
    CNTEdit: TEdit;
    HelpButton: TButton;
    Button1Send: TButton;
    Install: TMemo;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    PageControl1: TPageControl;
    Panel2: TPanel;
    SendButton: TButton;
    LogButton: TButton;
    PortButton: TButton;
    LineEndBox2: TComboBox;
    WMBButton: TButton;
    SendEdit1: TEdit;
    ReceiveMemo1: TMemo;
    Panel1: TPanel;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    ConsoleTab: TTabSheet;
    installTab: TTabSheet;
    wmbStringGrid1: TStringGrid;
    wmbusTab: TTabSheet;


    procedure AddButtonClick(Sender: TObject);

    procedure ADREditEditingDone(Sender: TObject);
    procedure Button1SendClick(Sender: TObject);
    procedure CIEditChange(Sender: TObject);
    procedure CmdComboBoxChange(Sender: TObject);
    procedure CmdComboBoxEditingDone(Sender: TObject);
    procedure CNTEditChange(Sender: TObject);
    procedure ConnectClick(Sender: TObject);
    procedure MANIDEditChange(Sender: TObject);
    procedure MANIDEditEditingDone(Sender: TObject);
    procedure TYPEEditChange(Sender: TObject);
    procedure TYPEEditEditingDone(Sender: TObject);
    procedure VEREditChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure HelpClick(Sender: TObject);
    procedure LogClick(Sender: TObject);
    procedure NewButtonClick(Sender: TObject);
    procedure ReceiveMemo1KeyDown(Sender: TObject; var Key: word;
      Shift: TShiftState);
    procedure ReceiveMemo1KeyPress(Sender: TObject; var Key: char);
    procedure ReceiveMemo1KeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
    procedure ReceiveMemo1UTF8KeyPress(Sender: TObject; var UTF8Key: TUTF8Char);
    procedure PKTButtonClick(Sender: TObject);
    procedure VEREditEditingDone(Sender: TObject);
    procedure VIFEditChange(Sender: TObject);
    procedure WMBButtonClick(Sender: TObject);
    //procedure SdpoSerial1RxData(Sender: TObject);
    procedure SendClick(Sender: TObject);
    procedure SendEdit1KeyDown(Sender: TObject; var Key: word; Shift: TShiftState);
    //procedure wmbusTabContextPopup(Sender: TObject; MousePos: TPoint;
    //  var Handled: boolean);
  private
    { private declarations }
    SerialPort: TnaykSerial; // - компонент ком-порта
    // События ком-порта:
    procedure SerialPortRxData(Sender: TObject);
    procedure SerialPortBeforeOpen(Sender: TObject);
    procedure SerialPortAfterOpen(Sender: TObject);
    procedure SerialPortBeforeClose(Sender: TObject);
    procedure SerialPortAfterClose(Sender: TObject);
    procedure SerialPortError(Sender: TObject; ErrorCode: integer; ErrorMessage: string);

  public
    { public declarations }
    SettingsFileName: string;
    procedure LoadSettings(const FileName: string);
    procedure SaveSettings(const FileName: string);
    procedure AddToLog(const Str: string);
    function PortIsOpen: boolean;
    function OpenPort: boolean;
    function ClosePort: boolean;
    function SetupPort: boolean;
    function GetPortSettingsStr: string;
    //    function WriteToPort(buf: array of byte; Count: integer): integer;
  end;

var
  Form1: TForm1;
  wmbRow: integer;
  wmbString: string;
  TermText: string;
  TermFile: string;
  //   AES-128
  aesIV, aesKey, aesBuf: array[0..15] of byte;
  //  Time
  CurrentTime: TDateTime;
  pCurrentTime: PDateTime;
  Year, Month, Day, Dow: word;
  Hour, Minute, Second, MilliSecond: word;
  //SettingsFileName: string;
  TermLog: TextFile;
  RxBuf: array[0..PACKETMAX - 1] of byte;
  TxBuf: array[0..PACKETMAX - 1] of byte;
  WxBuf: array[0..PACKETMAX - 1] of byte;
  CxBuf: array[0..PACKETMAX - 1] of byte;

  wmbStr: string;
  pDIF, pVIF: array[0..9] of byte;
  pCNT: int64;
  wmbPkt: TwmbPacket;
  wmbBuf: PByte;
  wmbP: PwmbPacket;

implementation

//uses
//  wmbus;
//, DCPreg
{$R *.lfm}

procedure TForm1.ConnectClick(Sender: TObject);
begin
  if (PortButton.Caption = 'CLOSE') then
  begin
    PortButton.Caption := 'PORT';
    SerialPort.Close;
    Exit;
  end;
  if SetupPort then
  begin
    SerialPort.Open;
    if (SerialPort.Connected) then
    begin
      ReceiveMemo1.Lines.Add('OPEN : ' + SerialPort.GetSettingsStr);
      PortButton.Caption := 'CLOSE';
    end;
  end;
end;

procedure TForm1.MANIDEditChange(Sender: TObject);
begin

end;

procedure TForm1.MANIDEditEditingDone(Sender: TObject);
begin
  wmbPkt.MANIDF := wmbMAN_ID(MANIDEdit.Text);
end;

procedure TForm1.TYPEEditChange(Sender: TObject);
begin

end;

procedure TForm1.TYPEEditEditingDone(Sender: TObject);
begin
  wmbPkt.TYPEF := StrToInt(TYPEEdit.Text);
end;

procedure TForm1.VEREditChange(Sender: TObject);
begin

end;

procedure TForm1.FormCreate(Sender: TObject);
var
  i: integer;
begin
  TermText := #10 + #13;
  TermFile := '';
  wmbRow := 0;
  // Load wmbCommand
  for i := 0 to Length(wmbCMDlist) - 1 do
  begin
    CmdComboBox.Items.Add(wmbCMDlist[i].CMDname);
  end;
  CmdComboBox.ItemIndex := 0;
  CmdComboBox.Hint := wmbCMDlist[CmdComboBox.ItemIndex].CMDhint;
  //  CmdComboBox.Text := IntToHex(wmbCMDlist[CmdComboBox.ItemIndex].CMDhex, 2) + 'h';
  wmbP := @wmbPkt;
  wmbBuf := @wmbPkt;
  //IntToHex(wmbCMDlist[CmdComboBox.ItemIndex].CMDhex, 2) + 'h';
  wmbPkt.LF := L_BLOCK0 - 3;
  wmbPkt.CF := wmbCMDlist[CmdComboBox.ItemIndex].CMDhex;
  //  pCF := wmbCMDlist[CmdComboBox.ItemIndex].CMDhex;
  //  Install.Lines.Add('CF ' + IntToStr(pCF) + ' Ind ' + IntToStr(CmdComboBox.ItemIndex));
  wmbPkt.MANIDF := wmbMAN_ID(MANIDEdit.Text);
  wmbPkt.ADDRF := wmbBcdToBin(ADREdit.Text);
  wmbPkt.VERF := StrToInt(VEREdit.Text);
  wmbPkt.TYPEF := StrToInt(TYPEEdit.Text);
  wmbPkt.CIF := wmbHexToBin(CIEdit.Text);
  if wmbPkt.CIF > 0 then
    wmbPkt.LF := L_BLOCK0 - 2;
  // имя файла настроек:
  SettingsFileName := GetUserDir;
  {$IFDEF MSWINDOWS}
  SettingsFileName := IncludeTrailingPathDelimiter(IncludeTrailingPathDelimiter(
    SettingsFileName + 'Application Data') + sSettingsDir);
  {$ELSE}
  SettingsFileName := IncludeTrailingPathDelimiter(IncludeTrailingPathDelimiter(
    SettingsFileName + '.config') + sSettingsDir);
  {$ENDIF}
  ForceDirectories(SettingsFileName);
  SettingsFileName := SettingsFileName + 'settings.cfg';

  // создаем экземпляр класса компорта и назначаем ему обработчики событий:
  SerialPort := TnaykSerial.Create(Self);
  SerialPort.OnRxChar := @SerialPortRxData;
  wmbString := '';
  //SerialPort.OnBeforeOpen := @SerialPortBeforeOpen;
  //SerialPort.OnAfterOpen := @SerialPortAfterOpen;
  //SerialPort.OnBeforeClose := @SerialPortBeforeClose;
  //SerialPort.OnAfterClose := @SerialPortAfterClose;
  //SerialPort.OnError := @SerialPortError;

  // читаем настройки порта из файла:
  SerialPort.LoadSettings(SettingsFileName);
  // Загрузка настроек для форм:
  //Form1.LoadSettings(SettingsFileName);
  //Form1.LoadSettings( SettingsFileName );
end;

procedure TForm1.Button1SendClick(Sender: TObject);
var
  str: string;

begin
  str := SendEdit1.Text;
  ReceiveMemo1.Lines.Add(str);

  if (TermFile <> '') then
    WriteLn(TermLog, str);

  case LineEndBox2.ItemIndex of
    0:
      str := str + #13;
    1:
      str := str + #10;
    2:
      str := str + #10 + #13;
    3:
      str := str + #9;
    4:
      str := str + #27;
  end;
  SerialPort.WriteStr(str);
  //  s:= #10 {клавиша <Enter>  + #10 + #13
  SendEdit1.Text := '';
  ReceiveMemo1.SelStart := Length(ReceiveMemo1.Text);
end;

procedure TForm1.CIEditChange(Sender: TObject);
begin
  wmbPkt.CIF := wmbHexToBin(CIEdit.Text);
  if wmbPkt.CIF > 0 then
    wmbPkt.LF := L_BLOCK0 - 2
  else
    wmbPkt.LF := L_BLOCK0 - 3;
end;

procedure TForm1.AddButtonClick(Sender: TObject);
var
  aStr: string;
  x: int64;
  valid: boolean;
  i, m: integer;
begin
  aStr := '';
  for i := 0 to 9 do
  begin
    pDIF[i] := 0;
    pVIF[i] := 0;
  end;

  if DIFEdit.Text <> '' then
  begin
    i := HexToBin(@DIFEdit.Text[1], @pDif, 10);
    //    pDIF := wmbHexToBin(DIFEdit.Text);
    aStr := wmbPrint(pDIF, i) + ';';
    Install.Lines.Add(aStr);
  end
  else
    Exit;
  if VIFEdit.Text <> '' then
  begin
    i := HexToBin(@VIFEdit.Text[1], @pVIF, 10);
    //    pDIF := wmbHexToBin(DIFEdit.Text);
    aStr := wmbPrint(pVIF, i) + ';';
    Install.Lines.Add(aStr);
  end
  else
    Exit;
  //  wmbAddAlarm(wmbBuf, 7);
  //  wmbAddAlarm(wmbBuf, 0);
  if CNTEdit.Text <> '' then
  begin
    pCNT := StrToInt(CNTEdit.Text);
    aStr := IntToStr(pCNT) + ';';
    Install.Lines.Add(aStr);
  end
  else if pDIF[0] <> 0 then
    Exit;
  wmbAddParam(wmbBuf, pDIF, pVIF, pCNT);
  //
    aStr := '';
  for i := 0 to 9 do
  begin
    pDIF[i] := 0;
    pVIF[i] := 0;
  end;

  if AlDIF.Text <> '' then
  begin
    i := HexToBin(@AlDIF.Text[1], @pDif, 10);
    //    pDIF := wmbHexToBin(DIFEdit.Text);
    aStr := wmbPrint(pDIF, i) + ';';
    Install.Lines.Add(aStr);
  end
  else
    Exit;
  if AlVIF.Text <> '' then
  begin
    i := HexToBin(@AlVIF.Text[1], @pVIF, 10);
    //    pDIF := wmbHexToBin(DIFEdit.Text);
    aStr := wmbPrint(pVIF, i) + ';';
    Install.Lines.Add(aStr);
  end
  else
    Exit;
  //  wmbAddAlarm(wmbBuf, 7);
  //  wmbAddAlarm(wmbBuf, 0);
  if AlValue.Text <> '' then
  begin
    pCNT := StrToInt(AlValue.Text);
    aStr := IntToStr(pCNT) + ';';
    Install.Lines.Add(aStr);
  end
  else if pDIF[0] <> 0 then
    Exit;
  wmbAddParam(wmbBuf, pDIF, pVIF, pCNT);

end;


procedure TForm1.ADREditEditingDone(Sender: TObject);
begin
  wmbPkt.ADDRF := wmbBcdToBin(ADREdit.Text);
end;

procedure TForm1.CmdComboBoxChange(Sender: TObject);
begin

end;

procedure TForm1.CmdComboBoxEditingDone(Sender: TObject);
begin
  //  CmdComboBox.Text := IntToHex(wmbCMDlist[CmdComboBox.ItemIndex].CMDhex, 2) + 'h';
  CmdComboBox.Hint := wmbCMDlist[CmdComboBox.ItemIndex].CMDhint;
  wmbPkt.CF := wmbCMDlist[CmdComboBox.ItemIndex].CMDhex;
end;

procedure TForm1.CNTEditChange(Sender: TObject);
begin

end;



procedure TForm1.HelpClick(Sender: TObject);
//var
//  F: TextFile;
//  S: string;
begin
  ReceiveMemo1.Lines.Add('UART Wireless M-Bus Terminal');
  //if OpenDialog1.Execute then
  //begin
  //  AssignFile(F, OpenDialog1.FileName);
  //  ReSet(F);
  //  ReadLn(F, S);
  //  while EOF(F) = False do
  //  begin
  //    ReceiveMemo1.Lines.Add(S);
  //    ReadLn(F, S);
  //    //    for X := 1 to Length(S) do
  //    //      if (S[X] = '*') or (S[X] = 'O') then
  //    //        T[X, Y] := True;
  //    //    if Length(S) > Max then
  //    //      Max := Length(S);
  //  end;
  //  CloseFile(F);
  //end;
end;

procedure TForm1.LogClick(Sender: TObject);
begin
  //LogFile:=;
  if (LogButton.Caption = 'STOP') then
  begin
    LogButton.Caption := 'LOG';
    CloseFile(TermLog);
    TermFile := '';
    Exit;
  end;

  if OpenDialog1.Execute then
  begin
    TermFile := OpenDialog1.FileName;
    AssignFile(TermLog, TermFile);
    //Open file for reading
    //    Reset(f1); {открываем первый файл для чтения}
    //Rewrite(f1);  {открываем файл для записи}
    if (FileExists(TermFile)) then
      Append(TermLog)
    else
      Rewrite(TermLog);
    //Rewrite(TermLog);
    //SeekEof(TermLog);
    //CloseFile(TermLog);
    //Append(TermLog);
    //While we are not at the end of the file ...
    //while not EOF(TermLog) do
    //  ReadLn(TermLog, str);
    LogButton.Caption := 'STOP';
  end;
  //CloseFile(LogFile);
end;

procedure TForm1.NewButtonClick(Sender: TObject);
var
  i: integer;
begin
  wmbP := @wmbPkt;
  wmbBuf := @wmbPkt;
  for i := 0 to 9 do
  begin
    pDIF[i] := 0;
    pVIF[i] := 0;
  end;
  //IntToHex(wmbCMDlist[CmdComboBox.ItemIndex].CMDhex, 2) + 'h';
  wmbPkt.LF := L_BLOCK0 - 3;
  wmbPkt.CF := wmbCMDlist[CmdComboBox.ItemIndex].CMDhex;
  //  pCF := wmbCMDlist[CmdComboBox.ItemIndex].CMDhex;
  //  Install.Lines.Add('CF ' + IntToStr(pCF) + ' Ind ' + IntToStr(CmdComboBox.ItemIndex));
  wmbPkt.MANIDF := wmbMAN_ID(MANIDEdit.Text);
  wmbPkt.ADDRF := wmbBcdToBin(ADREdit.Text);
  wmbPkt.VERF := StrToInt(VEREdit.Text);
  wmbPkt.TYPEF := StrToInt(TYPEEdit.Text);

  wmbPkt.CIF := wmbHexToBin(CIEdit.Text);
  if wmbPkt.CIF > 0 then
    wmbPkt.LF := L_BLOCK0 - 2;
end;


procedure TForm1.ReceiveMemo1KeyDown(Sender: TObject; var Key: word;
  Shift: TShiftState);
begin
  // (Key = VK_RETURN) then
  //if (Key = VK_RETURN) then   //  Form1.Button1SendClick(nil);
  //else   SendEdit1.Text := SendEdit1.Text + char(Key);   //  Form1.Button1SendClick(nil);
end;

procedure TForm1.ReceiveMemo1KeyPress(Sender: TObject; var Key: char);
begin
end;

procedure TForm1.ReceiveMemo1KeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
begin
  if (Key = VK_RETURN) then
    Form1.Button1SendClick(nil);
  if (Key = VK_CONTROL) then
  begin
    SendEdit1.Text := SendEdit1.Text + #17;
    Form1.Button1SendClick(nil);
  end;

end;

procedure TForm1.ReceiveMemo1UTF8KeyPress(Sender: TObject; var UTF8Key: TUTF8Char);
begin
  if ((UTF8Key <> #13) or (UTF8Key <> #17)) then
  begin
    SendEdit1.Text := SendEdit1.Text + UTF8Key;
  end;
end;

procedure TForm1.PKTButtonClick(Sender: TObject);
var
  wStr: string;
  i, iP, iFlags: integer;
  wP: PByte;
  TSec: Int64;
begin
  wmbP := @wmbPkt;
  wP := @wmbPkt;
  wmbBuf := @wmbPkt;
  if TimeCheckBox.Checked then
  begin
    wmbDataIns(wP, $04, 1);
    wmbDataIns(wP, $6D, 1);
    CurrentTime := Now;

    pCurrentTime := @CurrentTime;

    TSec := StrToInt(FormatDateTime('ss', pCurrentTime^)) +
    60*StrToInt(FormatDateTime('nn', pCurrentTime^)) +
    3600*StrToInt(FormatDateTime('hh', pCurrentTime^));
    wmbDataIns(wP, TSec, 4);
//    ADREdit.Text:=IntToStr(TSec);

   // wmbDataIns(wP, StrToInt(FormatDateTime('ss', pCurrentTime^)), 1);
   // wmbDataIns(wP, StrToInt(FormatDateTime('nn', pCurrentTime^)), 1);
   // wmbDataIns(wP, StrToInt(FormatDateTime('hh', pCurrentTime^)), 1);
   // wmbDataIns(wP, DayOfWeek(pCurrentTime^) - 1, 1);
   // wmbDataIns(wP, StrToInt(FormatDateTime('DD', pCurrentTime^)), 1);
   // wmbDataIns(wP, StrToInt(FormatDateTime('MM', pCurrentTime^)), 1);
   // wmbDataIns(wP, StrToInt(FormatDateTime('YYYY', pCurrentTime^)), 2);
  end;

  wmbStr := 'CRT=' + wmbPrint(wmbBuf, wmbBuf^);
  Install.Lines.Add(wmbStr);
  iP := wmbAddCRC(wmbBuf);
  wmbStr := 'ATF=' + wmbPrint(wmbBuf, iP);
  Install.Lines.Add(wmbStr);
  wStr := 'Check : ';
  iP := wmbCheck(wmbBuf, iP);
  wStr := wStr + IntToStr(iP) + ' ';
  wStr := wStr + wmbPrint(wmbBuf, iP) + #13;
  Install.Lines.Add(wStr);
  SendEdit1.Text := wmbStr;
end;

procedure TForm1.VEREditEditingDone(Sender: TObject);
begin
  wmbPkt.VERF := StrToInt(VEREdit.Text);
end;

procedure TForm1.VIFEditChange(Sender: TObject);
begin

end;

//function StrToHex(S: String): String;
//var I: Integer;
//begin
//   Result:= '';
//   for I := 1 to length (S) do
//     Result:= Result+IntToHex(ord(S[i]),2);
//end;
//ATF=1E44C42E7856341201075DCB780414B17F390501FD1700076D5B74C5AAFB27955FE4406525
//ATF=1E44C42E7856341201075DCB780414B17F390501FD1700076DACACF762482F955FE4402C25
//ATF=145300001111111103A4840B7804140180000001FD1700AC3B
//ATF=1453C42E78563412010798A77804140180000001FD1700AC3B
//ATF=140800001111111103A4422D7804140180000001FD1700AC3B
//ATF=1253C42E020000000131E019510F7F0A0001000300604A


procedure TForm1.WMBButtonClick(Sender: TObject);
var
  wStr, str: string;
  //wrkBuf: array[0..PACKETMAX - 1] of byte;
  p, iCh, i, iRx, numStr: integer;
  pPkt, iW: integer;
  sDIF, sVIF, sDATA: string;

begin
  numStr := 0;
  if wmbString = '' then
  begin
    wStr := SendEdit1.Text;
    //    SendEdit1.Text := '';
  end
  else
  begin
    wStr := wmbString;
    //    wmbString := '';
  end;
  //wStr := SendEdit1.Text;//wmbString;
  p := Pos('=', wStr);
  if p <= 0 then
  begin
    //    Install.Lines.Add('No ATF');
    Exit;
    //uStr := 'ATF=1253C42E020000000131E019510F7F0A0001000300604A';
  end;

  iRx := HexToBin(@wStr[p + 1], @rxBuf, PACKETMAX);
  str := 'RCV : ' + IntToStr(iRx) + ' : ' + wmbPrint(rxBuf, iRx);
  Install.Lines.Add(str);

  iCh := wmbCheck(rxBuf, iRx);
  //  Install.Lines.Add('Check : ' + IntToStr(iCh));
  if iCh = 0 then
    Exit;
  iW := wmbDelCRC(rxBuf, iCh);
  //  Install.Lines.Add('DEL ' + IntToStr(iW));
  if iW < 10 then
    Exit;
  // Parse Packet
  wmbP := @rxBuf;
  wmbBuf := @rxBuf;
  ////   Insert Time
  CurrentTime := Now;
  pCurrentTime := @CurrentTime;
  str := FormatDateTime('hh:nn:ss DD.MM.YYYY', pCurrentTime^);
  Install.Lines.Add(str);
  Install.Lines.Add(wStr);

  //str := FormatDateTime('ddd', pCurrentTime^);
  //  Install.Lines.Add('DDD ' + IntToStr(DayOfWeek(pCurrentTime^)));
  Install.SelStart := Length(Install.Text);
  //  if wmbRow = 0 then
  //  begin
  Inc(wmbRow);
  wmbStringGrid1.InsertColRow(False, wmbRow);
  //  end;

  //iFlags := 1;
  wmbStringGrid1.Row := wmbRow;
  wmbStringGrid1.Col := 0;
  // Time
  wmbStringGrid1.Cells[0, wmbRow] := str;
  // Command
  pPkt := 1;
  wmbStringGrid1.Cells[1, wmbRow] := IntToHex(wmbP^.CF, 2) + 'h';
  Inc(pPkt);
  // ManID
  wmbStringGrid1.Cells[2, wmbRow] := wmbMAN_ID_TO_STR(wmbP^.MANIDF);
  pPkt := pPkt + 2;
  // Addr
  wmbStringGrid1.Cells[3, wmbRow] := IntToHex(wmbP^.ADDRF, 8);
  pPkt := pPkt + 4;
  // Ver
  wmbStringGrid1.Cells[4, wmbRow] := IntToHex(wmbP^.VERF, 2) + 'h';
  Inc(pPkt);
  // Type
  wmbStringGrid1.Cells[5, wmbRow] := IntToHex(wmbP^.TYPEF, 2) + 'h';
  Inc(pPkt);
  if pPkt >= iW then
    Exit;
  // CI
  wmbStringGrid1.Cells[6, wmbRow] := IntToHex(wmbP^.CIF, 2) + 'h';
  Inc(pPkt);
  //if pPkt >= iW then
  //  Exit;

  if wmbP^.CIF = $72 then
    wmbProcess72(wmbBuf, iW, @pPkt)
  else if wmbP^.CIF = $51 then
    wmbProcess51(wmbBuf, iW, @pPkt)
  else if wmbP^.CIF = $7A then
    wmbProcess7A(wmbBuf, iW, @pPkt)
  else if wmbP^.CIF = $71 then
  begin
    Install.Lines.Add('Alarm Process');
    wmbProcess71(wmbBuf, iW, @pPkt);
  end
  else if wmbP^.CIF <> $78 then
  begin
    Install.Lines.Add('Bad Packet Type');
    Exit;
  end;
  while pPkt < iW do
  begin
    if wmbGetRecord(wmbBuf, iW, @pPkt, sDif, sVif, sData) > 0 then
    begin
      Inc(numStr);
      wmbStringGrid1.Row := wmbRow;
      wmbStringGrid1.Cells[7, wmbRow] := sDif;
      wmbStringGrid1.Cells[8, wmbRow] := sVif;
      wmbStringGrid1.Cells[9, wmbRow] := sData;
      Inc(wmbRow);
      wmbStringGrid1.InsertColRow(False, wmbRow);
    end;
  end;
  if numStr > 0 then
  begin
    wmbStringGrid1.DeleteColRow(False, wmbRow);
    Dec(wmbRow);
  end;
end;

procedure TForm1.SerialPortRxData(Sender: TObject);
var
  readBuf: array[0..2047] of byte;
  i: integer;
begin
  for i := 0 to SerialPort.Read(readBuf, 2048) - 1 do
  begin
    wmbString := wmbString + char(readBuf[i]);
    if (readBuf[i] = 10) or (readBuf[i] = 13) then
    begin
      WMBButton.Click;
      ReceiveMemo1.Text := ReceiveMemo1.Text + wmbString;
      ReceiveMemo1.SelStart := Length(ReceiveMemo1.Text);
      if (TermFile <> '') then
        WriteLn(TermLog, wmbString);
      wmbString := '';
    end;
  end;
end;

procedure TForm1.SendClick(Sender: TObject);
var
  F: TextFile;
  S: string;
begin
  if OpenDialog1.Execute then
  begin
    AssignFile(F, OpenDialog1.FileName);
    ReSet(F);
    //ReadLn(F, S);
    while (not EOF(F)) do
    begin
      //ReceiveMemo1.Lines.Add(S);
      ReadLn(F, S);
      SerialPort.WriteStr(S + #13 + #10);
    end;
    CloseFile(F);
  end;
  ReceiveMemo1.SelStart := Length(ReceiveMemo1.Text);
end;

procedure TForm1.SendEdit1KeyDown(Sender: TObject; var Key: word; Shift: TShiftState);
begin
  if (Key = VK_RETURN) then
    Form1.Button1SendClick(nil);
end;

procedure TForm1.LoadSettings(const FileName: string);
begin
  // Загрузка настроек
  //with TIniFile.Create(FileName) do
  //  try
  //    Width := ReadInteger('FormLog', 'Width', 440);
  //    Height := ReadInteger('FormLog', 'Height', 300);
  //    Top := ReadInteger('FormLog', 'Top', 200);
  //    Left := ReadInteger('FormLog', 'Left', 300);
  //  finally
  //    Free;
  //  end;
end;

procedure TForm1.AddToLog(const Str: string);
begin
  //ReceiveMemo1.Lines.BeginUpdate;
  //if ReceiveMemo1.Lines.Count > 1500 then
  //  while ReceiveMemo1.Lines.Count > 1000 do
  //    ReceiveMemo1.Lines.Delete(0);
  //ReceiveMemo1.Lines.EndUpdate;
  //ReceiveMemo1.Lines.Add(Str);
end;


procedure TForm1.SaveSettings(const FileName: string);
begin
  // Сохранение настроек
  //with TIniFile.Create(FileName) do
  //  try
  //    WriteInteger('FormLog', 'Width', Width);
  //    WriteInteger('FormLog', 'Height', Height);
  //    WriteInteger('FormLog', 'Top', Top);
  //    WriteInteger('FormLog', 'Left', Left);
  //  finally
  //    Free;
  //  end;
end;

procedure TForm1.SerialPortBeforeOpen(Sender: TObject);
begin
  // событие перед открытием порта

end;

procedure TForm1.SerialPortAfterOpen(Sender: TObject);
begin
  // событие после открытия порта
  //Form1.AddToLog('Порт открыт.');
  //PortItem.Caption := 'Закрыть порт';
  //FormMain.MainStatusBar.Panels.Items[3].Text := 'Открыт';
  //PortSetupItem.Enabled := False;

  //SetupItem.Enabled := PortSetupItem.Enabled;
  //FormMain.PortToolButton.Hint := PortItem.Caption;
  //FormMain.PortSetupToolButton.Enabled := PortSetupItem.Enabled;
  //FormMain.SetupToolButton.Enabled := SetupItem.Enabled;
end;

procedure TForm1.SerialPortBeforeClose(Sender: TObject);
begin
  // событие перед закрытием порта

end;

procedure TForm1.SerialPortAfterClose(Sender: TObject);
begin
  // событие после закрытия порта
  //FormLog.AddToLog('Порт закрыт.');
  //PortItem.Caption := 'Открыть порт';
  //FormMain.MainStatusBar.Panels.Items[3].Text := 'Закрыт';
  //PortSetupItem.Enabled := True;

  //SetupItem.Enabled := PortSetupItem.Enabled;
  //FormMain.PortToolButton.Hint := PortItem.Caption;
  //FormMain.PortSetupToolButton.Enabled := PortSetupItem.Enabled;
  //FormMain.SetupToolButton.Enabled := SetupItem.Enabled;
end;

procedure TForm1.SerialPortError(Sender: TObject; ErrorCode: integer;
  ErrorMessage: string);
begin
  // Событие при ошибке порта
  //  FormLog.AddToLog('Ошибка порта ' + IntToStr(ErrorCode) + ': ' + ErrorMessage);
end;

function TForm1.PortIsOpen: boolean;
begin
  Result := (Assigned(SerialPort) and SerialPort.Connected);
end;

function TForm1.OpenPort: boolean;
begin
  if not Assigned(SerialPort) then
  begin
    Result := False;
    Exit;
  end;
  SerialPort.Open;
  Result := SerialPort.Connected;
end;

function TForm1.ClosePort: boolean;
begin
  if not Assigned(SerialPort) then
  begin
    Result := False;
    Exit;
  end;
  SerialPort.Close;
  Result := not SerialPort.Connected;
end;

function TForm1.SetupPort: boolean;
begin
  Result := SerialPort.SetupPortDialog(SettingsFileName);
end;

function TForm1.GetPortSettingsStr: string;
begin
  Result := SerialPort.GetSettingsStr;
end;

//function TForm1.WriteToPort(buf: array of byte; Count: integer): integer;
//var
//  str: string;
//  i: integer;
//begin
//  // запись массива байт в порт
//  Result := 0;
//  if not SerialPort.Connected then
//    Exit;

//  Result := SerialPort.Write(buf, Count);



//  str := '>>';
//  for i := 0 to Count - 1 do
//  begin
//    str := str + ' ' + IntToHex(buf[i], 2);
//  end;
//  Form1.AddToLog(str);
//end;


end.
