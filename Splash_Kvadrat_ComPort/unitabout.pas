unit unitAbout;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls;

type

  { TFormAbout }

  TFormAbout = class(TForm)
    Button1: TButton;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    VerLabel: TLabel;
    AuthorLabel: TLabel;
    MailLabel: TLabel;
    IcqLabel: TLabel;
    TitleLabel: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

implementation

uses unitData;

{$R *.lfm}

{ TFormAbout }

procedure TFormAbout.FormCreate(Sender: TObject);
begin
  TitleLabel.Caption := sAppTitle;
  VerLabel.Caption := sVer;
  AuthorLabel.Caption := sAuthor;
  MailLabel.Caption := sMail;
  IcqLabel.Caption := sIcq;
  Button1.Left:=(ClientWidth - Button1.Width) div 2;
end;

end.

