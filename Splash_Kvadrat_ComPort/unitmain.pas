unit unitMain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ComCtrls, IniFiles;

type

  { TFormMain }

  TFormMain = class(TForm)
    MainStatusBar: TStatusBar;
    MainToolBar: TToolBar;
    PortSetupToolButton: TToolButton;
    SetupToolButton: TToolButton;
    ToolButton1: TToolButton;
    PortToolButton: TToolButton;
    ToolButton2: TToolButton;
    LogToolButton: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure LogToolButtonClick(Sender: TObject);
    procedure PortSetupToolButtonClick(Sender: TObject);
    procedure PortToolButtonClick(Sender: TObject);
    procedure SetupToolButtonClick(Sender: TObject);
  private
    { private declarations }
    FirstShow: boolean;
  public
    { public declarations }
    procedure UpdateStatusPanel;
    procedure LoadSettings(const FileName: string);
    procedure SaveSettings(const FileName: string);
  end;

var
  FormMain: TFormMain;

implementation

uses
  UnitData;

{$R *.lfm}

procedure TFormMain.FormCreate(Sender: TObject);
begin
  FirstShow := True;
  Application.Title := sAppTitle;
  Caption := Application.Title;
end;

procedure TFormMain.LoadSettings(const FileName: string);
begin
  // Загрузка настроек
  with TIniFile.Create(FileName) do
    try
      MainToolBar.Visible := ReadBool('Settings', 'ShowToolBar', True);
      if Assigned(Data) then
        Data.PanelItem.Checked := MainToolBar.Visible;

    finally
      Free;
    end;
end;

procedure TFormMain.SaveSettings(const FileName: string);
begin
  // Сохранение настроек
  with TIniFile.Create(FileName) do
    try

    finally
      Free;
    end;
end;

procedure TFormMain.FormShow(Sender: TObject);
begin
  if FirstShow then
  begin
    FirstShow := False;
    UpdateStatusPanel;
  end;
end;

procedure TFormMain.LogToolButtonClick(Sender: TObject);
begin
  Data.ConsoleItem.Click;
end;

procedure TFormMain.PortSetupToolButtonClick(Sender: TObject);
begin
  Data.PortSetupItem.Click;
end;

procedure TFormMain.PortToolButtonClick(Sender: TObject);
begin
  Data.PortItem.Click;
end;

procedure TFormMain.SetupToolButtonClick(Sender: TObject);
begin
  Data.SetupItem.Click;
end;

procedure TFormMain.UpdateStatusPanel;
begin
  if not Assigned(Data) then
    Exit;
  MainStatusBar.Panels.Items[1].Text := Data.GetPortSettingsStr;

  if Data.PortIsOpen then
    MainStatusBar.Panels.Items[3].Text := 'Открыт'
  else
    MainStatusBar.Panels.Items[3].Text := 'Закрыт';
end;

end.
