{***********************************************************************
  Компонент для работы с COM-портом.
  В основной программе нужно подключить этот модуль (добавить в Uses)
  и создать экземпляр класса TnaykSerial. Дальше работать с этим
  созданным экземпляром.
***********************************************************************}

//old Delphi does not have MSWINDOWS define.
{$IFDEF WIN32}
  {$IFNDEF MSWINDOWS}
    {$DEFINE MSWINDOWS}
  {$ENDIF}
{$ENDIF}

{$IFDEF LiNUX}
  {$IFNDEF UseCThreads}
    {$DEFINE UseCThreads}
  {$ENDIF}
{$ENDIF}

{$IFDEF FPC}
  {$MODE DELPHI}
  {define working mode w/o LIBC for fpc}
  {$DEFINE NO_LIBC}
{$ENDIF}
{$Q-}
{$H+}
{$M+}

unit naykSerial;

interface

uses
{$IFNDEF MSWINDOWS}
  {$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}
  {$IFNDEF NO_LIBC}
  Libc, KernelIoctl,
  {$ELSE}
  termio,
  {$ENDIF}
  Unix, BaseUnix,
  {$IFNDEF FPC}
  Types,
  {$ENDIF}
  Process,
{$ELSE}
  Windows, Messages, Registry,
  {$IFDEF FPC}
  winver,
  {$ENDIF}
{$ENDIF}
{$IFDEF CIL}
  System.IO,
{$ENDIF}
  Classes, SysUtils, Forms, Controls, StdCtrls, Graphics, Dialogs, IniFiles;

{$IFNDEF MSWINDOWS}
const
  INVALID_HANDLE_VALUE = THandle(-1);
  DTR_CONTROL_DISABLE = 0;
  DTR_CONTROL_ENABLE = 1;
  DTR_CONTROL_HANDSHAKE = 2;
  RTS_CONTROL_DISABLE = 0;
  RTS_CONTROL_ENABLE = 1;
  RTS_CONTROL_HANDSHAKE = 2;
  RTS_CONTROL_TOGGLE = 3;

  CS7fix = $0000020;

  MaxRates = 11;
  Rates: array[0..MaxRates, 0..1] of cardinal =
  (
    (0, B0),
    (110, B110),
    (300, B300),
    (600, B600),
    (1200, B1200),
    (2400, B2400),
    (4800, B4800),
    (9600, B9600),
    (19200, B19200),
    (38400, B38400),
    (57600, B57600),
    (115200, B115200)
  );

  EV_RXCHAR = 1;
  EV_RXFLAG = 2;
  EV_CTS = 8;
  EV_DSR = $10;
  EV_RLSD = $20;
  EV_BREAK = $40;
  EV_ERR = $80;
  EV_RING = $100;

type
  TDCB = record
    DCBlength: DWORD;
    BaudRate: DWORD;
    Flags: Longint;
    wReserved: Word;
    XonLim: Word;
    XoffLim: Word;
    ByteSize: Byte;
    Parity: Byte;
    StopBits: Byte;
    XonChar: CHAR;
    XoffChar: CHAR;
    ErrorChar: CHAR;
    EofChar: CHAR;
    EvtChar: CHAR;
    wReserved1: Word;
  end;
  PDCB = ^TDCB;

{$ENDIF}

const
  sOK = 0;
  sErr = integer(-1);

  ErrAlreadyOwned = 9991;
  ErrAlreadyInUse = 9992;
  ErrWrongParameter = 9993;
  ErrPortNotOpen = 9994;
  ErrNoDeviceAnswer =  9995;
  ErrMaxBuffer = 9996;
  ErrTimeout = 9997;
  ErrNotRead = 9998;
  ErrFrame = 9999;
  ErrOverrun = 10000;
  ErrRxOver = 10001;
  ErrRxParity = 10002;
  ErrTxFull = 10003;
  ErrAsyncNil = 10004;
  ErrStoreSettings = 10005;
  ErrLoadSettings = 10006;

type
  // various types
  TPort = string;
  TBaudRate = (brCustom, br110, br300, br600, br1200, br2400, br4800, br9600, br19200, br38400, br57600, br115200);
  TStopBits = (sbOneStopBit, sbOne5StopBits, sbTwoStopBits);
  TDataBits = (dbFive, dbSix, dbSeven, dbEight);
  TParityBits = (prNone, prOdd, prEven, prMark, prSpace);
  TDTRFlowControl = (dtrDisable, dtrEnable, dtrHandshake);
  TRTSFlowControl = (rtsDisable, rtsEnable, rtsHandshake, rtsToggle);
  TFlowControl = (fcHardware, fcSoftware, fcNone, fcCustom);
  TComEvent = (evRxChar, evRing, evCTS, evDSR, evRLSD, evError);
  TComEvents = set of TComEvent;
  TComSignal = (csCTS, csDSR, csRing, csRLSD);
  TComSignals = set of TComSignal;
  TStoredProp = (spBasic, spFlowControl, spBuffer, spTimeouts, spParity, spOthers);
  TStoredProps = set of TStoredProp;
  TComSignalEvent = procedure(Sender: TObject; OnOff: Boolean) of object;
  TComErrorEvent = procedure(Sender: TObject; ErrorCode: integer; ErrorMessage: String) of object;

type

{$IFDEF MSWINDOWS}
  // types for asynchronous calls
  TOperationKind = (okWrite, okRead);
  TAsync = record
    Overlapped: TOverlapped;
    Kind: TOperationKind;
    Data: Pointer;
    Size: Integer;
  end;
  PAsync = ^TAsync;
{$ENDIF}

  ESynaSerError = class(Exception)
  public
    ErrorCode: integer;
    ErrorMessage: string;
  end;

  // TnaykSerial component and asistant classes --------------------------------
  TCustomComPort = class; // forward declaration

  // thread for background monitoring of port events
  TComThread = class(TThread)
  private
    FComPort: TCustomComPort;
    {$IFDEF MSWINDOWS}
    FStopEvent: THandle;
    {$ELSE}
    FStop: boolean;
    {$ENDIF}
    FEvents: TComEvents;
  protected
    procedure DispatchComMsg;
    procedure DoEvents;
    procedure Execute; override;
    procedure Stop;
  public
    constructor Create(AComPort: TCustomComPort);
    destructor Destroy; override;
  end;

{$IFDEF MSWINDOWS}
  // timoeout properties for read/write operations
  TComTimeouts = class(TPersistent)
  private
    FComPort: TCustomComPort;
    FReadInterval: Integer;
    FReadTotalM: Integer;
    FReadTotalC: Integer;
    FWriteTotalM: Integer;
    FWriteTotalC: Integer;
    procedure SetComPort(const AComPort: TCustomComPort);
    procedure SetReadInterval(const Value: Integer);
    procedure SetReadTotalM(const Value: Integer);
    procedure SetReadTotalC(const Value: Integer);
    procedure SetWriteTotalM(const Value: Integer);
    procedure SetWriteTotalC(const Value: Integer);
  protected
    procedure AssignTo(Dest: TPersistent); override;
  public
    constructor Create;
    property ComPort: TCustomComPort read FComPort;
  published
    property ReadInterval: Integer read FReadInterval write SetReadInterval default -1;
    property ReadTotalMultiplier: Integer read FReadTotalM write SetReadTotalM default 0;
    property ReadTotalConstant: Integer read FReadTotalC write SetReadTotalC default 0;
    property WriteTotalMultiplier: Integer read FWriteTotalM write SetWriteTotalM default 100;
    property WriteTotalConstant: Integer read FWriteTotalC write SetWriteTotalC default 1000;
  end;
{$ENDIF}

  // flow control settings
  TComFlowControl = class(TPersistent)
  private
    FComPort: TCustomComPort;
    FOutCTSFlow: Boolean;
    FOutDSRFlow: Boolean;
    FControlDTR: TDTRFlowControl;
    FControlRTS: TRTSFlowControl;
    FXonXoffOut: Boolean;
    FXonXoffIn:  Boolean;
    FDSRSensitivity: Boolean;
    FTxContinueOnXoff: Boolean;
    FXonChar: AnsiChar;
    FXoffChar: AnsiChar;
    procedure SetComPort(const AComPort: TCustomComPort);
    procedure SetOutCTSFlow(const Value: Boolean);
    procedure SetOutDSRFlow(const Value: Boolean);
    procedure SetControlDTR(const Value: TDTRFlowControl);
    procedure SetControlRTS(const Value: TRTSFlowControl);
    procedure SetXonXoffOut(const Value: Boolean);
    procedure SetXonXoffIn(const Value: Boolean);
    procedure SetDSRSensitivity(const Value: Boolean);
    procedure SetTxContinueOnXoff(const Value: Boolean);
    procedure SetXonChar(const Value: AnsiChar);
    procedure SetXoffChar(const Value: AnsiChar);
    procedure SetFlowControl(const Value: TFlowControl);
    function GetFlowControl: TFlowControl;
  protected
    procedure AssignTo(Dest: TPersistent); override;
  public
    constructor Create;
    property ComPort: TCustomComPort read FComPort;
  published
    property FlowControl: TFlowControl read GetFlowControl write SetFlowControl stored False;
    property OutCTSFlow: Boolean read FOutCTSFlow write SetOutCTSFlow;
    property OutDSRFlow: Boolean read FOutDSRFlow write SetOutDSRFlow;
    property ControlDTR: TDTRFlowControl read FControlDTR write SetControlDTR;
    property ControlRTS: TRTSFlowControl read FControlRTS write SetControlRTS;
    property XonXoffOut: Boolean read FXonXoffOut write SetXonXoffOut;
    property XonXoffIn:  Boolean read FXonXoffIn write SetXonXoffIn;
    property DSRSensitivity: Boolean read FDSRSensitivity write SetDSRSensitivity default False;
    property TxContinueOnXoff: Boolean read FTxContinueOnXoff write SetTxContinueOnXoff default False;
    property XonChar: AnsiChar read FXonChar write SetXonChar default #17;
    property XoffChar: AnsiChar read FXoffChar write SetXoffChar default #19;
  end;

  // parity settings
  TComParity = class(TPersistent)
  private
    FComPort: TCustomComPort;
    FBits: TParityBits;
    FCheck: Boolean;
    FReplace: Boolean;
    FReplaceChar: AnsiChar;
    procedure SetComPort(const AComPort: TCustomComPort);
    procedure SetBits(const Value: TParityBits);
    procedure SetCheck(const Value: Boolean);
    procedure SetReplace(const Value: Boolean);
    procedure SetReplaceChar(const Value: AnsiChar);
  protected
    procedure AssignTo(Dest: TPersistent); override;
  public
    constructor Create;
    property ComPort: TCustomComPort read FComPort;
  published
    property Bits: TParityBits read FBits write SetBits;
    property Check: Boolean read FCheck write SetCheck default False;
    property Replace: Boolean read FReplace write SetReplace default False;
    property ReplaceChar: AnsiChar read FReplaceChar write SetReplaceChar default #0;
  end;

  // buffer size settings
  TComBuffer = class(TPersistent)
  private
    FComPort: TCustomComPort;
    FInputSize: Integer;
    FOutputSize: Integer;
    procedure SetComPort(const AComPort: TCustomComPort);
    procedure SetInputSize(const Value: Integer);
    procedure SetOutputSize(const Value: Integer);
  protected
    procedure AssignTo(Dest: TPersistent); override;
  public
    constructor Create;
    property ComPort: TCustomComPort read FComPort;
  published
    property InputSize: Integer read FInputSize write SetInputSize default 2048;
    property OutputSize: Integer read FOutputSize write SetOutputSize default 2048;
  end;

  // main component
  TCustomComPort = class(TComponent)
  private
    FEventThread: TComThread;
    FThreadCreated: Boolean;
    FHandle: THandle;
    FLastError: integer;
    FLastErrorDesc: string;
    FUpdateCount: Integer;
    FTriggersOnRxChar: Boolean;
    FConnected: Boolean;
    FRaiseExcept: boolean;
    FBaudRate: TBaudRate;
    FCustomBaudRate: Integer;
    FPort: TPort;
    FStopBits: TStopBits;
    FDataBits: TDataBits;
    FDiscardNull: Boolean;
    FEvents: TComEvents;
    FBuffer: TComBuffer;
    FParity: TComParity;
    {$IFDEF MSWINDOWS}
    FTimeouts: TComTimeouts;
    {$ENDIF}
    FFlowControl: TComFlowControl;
    FStoredProps: TStoredProps;
    FOnRxChar: TNotifyEvent;
    FOnRing: TNotifyEvent;
    FOnCTSChange: TComSignalEvent;
    FOnDSRChange: TComSignalEvent;
    FOnRLSDChange: TComSignalEvent;
    FOnAfterOpen: TNotifyEvent;
    FOnAfterClose: TNotifyEvent;
    FOnBeforeOpen: TNotifyEvent;
    FOnBeforeClose: TNotifyEvent;
    FOnError: TComErrorEvent;
    procedure SetConnected(const Value: Boolean);
    procedure SetBaudRate(const Value: TBaudRate);
    procedure SetCustomBaudRate(const Value: Integer);
    procedure SetNewPort(const Value: TPort);
    procedure SetStopBits(const Value: TStopBits);
    procedure SetDataBits(const Value: TDataBits);
    procedure SetDiscardNull(const Value: Boolean);
    procedure SetParity(const Value: TComParity);
    {$IFDEF MSWINDOWS}
    procedure SetTimeouts(const Value: TComTimeouts);
    {$ENDIF}
    function SerialCheck(SerialResult: integer): integer;
    procedure ExceptCheck;
    function GetErrorDesc(ErrorCode: integer): string;
    procedure SetSynaError(ErrNumber: integer);
    procedure RaiseSynaError(ErrNumber: integer);
    procedure SetBuffer(const Value: TComBuffer);
    procedure SetFlowControl(const Value: TComFlowControl);
    procedure CheckSignals(Open: Boolean);
    procedure CallAfterOpen;
    procedure CallAfterClose;
    procedure CallBeforeOpen;
    procedure CallBeforeClose;
    procedure CallRxChar;
    procedure CallRing;
    procedure CallCTSChange;
    procedure CallDSRChange;
    procedure CallRLSDChange;
    procedure CallError;
  protected
    procedure Loaded; override;
    procedure DoAfterClose; dynamic;
    procedure DoAfterOpen; dynamic;
    procedure DoBeforeClose; dynamic;
    procedure DoBeforeOpen; dynamic;
    procedure DoRxChar; dynamic;
    procedure DoRing; dynamic;
    procedure DoCTSChange(OnOff: Boolean); dynamic;
    procedure DoDSRChange(OnOff: Boolean); dynamic;
    procedure DoRLSDChange(OnOff: Boolean); dynamic;
    procedure DoError(ErrorCode: integer; ErrorMessage: string);
    procedure StoreIniFile(IniFile: TIniFile); virtual;
    procedure LoadIniFile(IniFile: TIniFile); virtual;
    procedure CreateHandle; virtual;
    procedure DestroyHandle; virtual;
    procedure ApplyDCB; dynamic;
    {$IFDEF MSWINDOWS}
    procedure ApplyTimeouts; dynamic;
    {$ELSE}
    procedure TermiosToDcb(const term: termios; var dcb: TDCB);
    procedure DcbToTermios(const dcb: TDCB; var term: termios);
    function CanRead(TimeOut: integer): boolean;
    {$ENDIF}
    procedure ApplyBuffer; dynamic;
    procedure SetupComPort; virtual;
    {$IFDEF MSWINDOWS}
    function WriteAsync(const Buffer; Count: Integer; var AsyncPtr: PAsync): Integer;
    function WriteStrAsync(const Str: AnsiString; var AsyncPtr: PAsync): Integer;
    function ReadAsync(var Buffer; Count: Integer; var AsyncPtr: PAsync): Integer;
    function ReadStrAsync(var Str: AnsiString; Count: Integer; var AsyncPtr: PAsync): Integer;
    function WaitForAsync(var AsyncPtr: PAsync): Integer;
    function IsAsyncCompleted(AsyncPtr: PAsync): Boolean;
    procedure AbortAllAsync;
    {$ENDIF}
    procedure BaudRateBoxChange(Sender: TObject);
    procedure BaudRateEditKeyPress(Sender: TObject; var Key: char);
    procedure BaudRateEditExit(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure BeginUpdate;
    procedure EndUpdate;
    procedure StoreSettings(StoreTo: string);
    procedure LoadSettings(LoadFrom: string);
    function SetupPortDialog(const SettingsFileName: String): boolean;
    function GetSettingsStr: string;
    procedure Open;
    procedure Close;
    function InputCount: Integer;
    function OutputCount: Integer;
    function Signals: TComSignals;
    procedure SetDTR(OnOff: Boolean);
    procedure SetRTS(OnOff: Boolean);
    procedure SetBreak(Duration: Integer);
    procedure ClearBuffers;
    procedure ClearInputBuffer;
    procedure ClearOutputBuffer;
    function Write(const Buffer; Count: Integer): Integer;
    function WriteStr(const Str: AnsiString): Integer;
    function Read(var Buffer; Count: Integer): Integer;
    function ReadStr(var Str: AnsiString; Count: Integer): Integer;
    function GetPortsList(List: TStrings): integer;
    function GetBaudRatesList(List: TStrings): integer;
    function GetDataBitsList(List: TStrings): integer;
    function GetStopBitsList(List: TStrings): integer;
    function GetParityList(List: TStrings): integer;
    function GetFlowControlList(List: TStrings): integer;
    property Handle: THandle read FHandle;
    property StoredProps: TStoredProps read FStoredProps write FStoredProps;
    property Connected: Boolean read FConnected write SetConnected default False;
    property BaudRate: TBaudRate read FBaudRate write SetBaudRate;
    property CustomBaudRate: Integer read FCustomBaudRate write SetCustomBaudRate;
    property Port: TPort read FPort write SetNewPort;
    property Parity: TComParity read FParity write SetParity;
    property StopBits: TStopBits read FStopBits write SetStopBits;
    property DataBits: TDataBits read FDataBits write SetDataBits;
    property DiscardNull: Boolean read FDiscardNull write SetDiscardNull default False;
    property Events: TComEvents read FEvents write FEvents;
    property Buffer: TComBuffer read FBuffer write SetBuffer;
    property FlowControl: TComFlowControl read FFlowControl write SetFlowControl;
    {$IFDEF MSWINDOWS}
    property Timeouts: TComTimeouts read FTimeouts write SetTimeouts;
    {$ENDIF}
    property RaiseExcept: boolean read FRaiseExcept write FRaiseExcept;
    property OnAfterOpen: TNotifyEvent read FOnAfterOpen write FOnAfterOpen;
    property OnAfterClose: TNotifyEvent read FOnAfterClose write FOnAfterClose;
    property OnBeforeOpen: TNotifyEvent read FOnBeforeOpen write FOnBeforeOpen;
    property OnBeforeClose: TNotifyEvent read FOnBeforeClose write FOnBeforeClose;
    property OnRxChar: TNotifyEvent read FOnRxChar write FOnRxChar;
    property OnRing: TNotifyEvent read FOnRing write FOnRing;
    property OnCTSChange: TComSignalEvent read FOnCTSChange write FOnCTSChange;
    property OnDSRChange: TComSignalEvent read FOnDSRChange write FOnDSRChange;
    property OnRLSDChange: TComSignalEvent read FOnRLSDChange write FOnRLSDChange;
    property OnError: TComErrorEvent read FOnError write FOnError;
  end;

  // publish the properties
  TnaykSerial = class(TCustomComPort)
    property Connected;
    property BaudRate;
    property Port;
    property Parity;
    property StopBits;
    property DataBits;
    property DiscardNull;
    property Events;
    property Buffer;
    property FlowControl;
    {$IFDEF MSWINDOWS}
    property Timeouts;
    {$ENDIF}
    property RaiseExcept;
    property OnAfterOpen;
    property OnAfterClose;
    property OnBeforeOpen;
    property OnBeforeClose;
    property OnRxChar;
    property OnRing;
    property OnCTSChange;
    property OnDSRChange;
    property OnRLSDChange;
    property OnError;
  end;

// aditional procedures
{$IFDEF MSWINDOWS}
procedure InitAsync(var AsyncPtr: PAsync);
procedure DoneAsync(var AsyncPtr: PAsync);
{$ENDIF}
procedure EnumComPorts(Ports: TStrings);

// conversion functions
function StrToBaudRate(Str: string): TBaudRate;
function StrToStopBits(Str: string): TStopBits;
function StrToDataBits(Str: string): TDataBits;
function StrToParity(Str: string): TParityBits;
function StrToFlowControl(Str: string): TFlowControl;
function BaudRateToStr(BaudRate: TBaudRate): string;
function StopBitsToStr(StopBits: TStopBits): string;
function DataBitsToStr(DataBits: TDataBits): string;
function ParityToStr(Parity: TParityBits): string;
function FlowControlToStr(FlowControl: TFlowControl): string;

implementation

const
  // auxilary constants used not defined in windows.pas
  dcb_Binary           = $00000001;
  dcb_Parity           = $00000002;
  dcb_OutxCTSFlow      = $00000004;
  dcb_OutxDSRFlow      = $00000008;
  //dcb_DTRControl       = $00000030;
  dcb_DSRSensivity     = $00000040;
  dcb_TxContinueOnXoff = $00000080;
  dcb_OutX             = $00000100;
  dcb_InX              = $00000200;
  dcb_ErrorChar        = $00000400;
  dcb_Null             = $00000800;
  //dcb_RTSControl       = $00003000;
  //dcb_AbortOnError     = $00004000;
  //dcb_DtrControlDisable   = $00000000;
  //dcb_DtrControlEnable    = $00000010;
  //dcb_DtrControlHandshake = $00000020;
  //dcb_RtsControlDisable   = $00000000;
  //dcb_RtsControlEnable    = $00001000;
  dcb_RtsControlHandshake = $00002000;

(*****************************************
 * auxilary functions and procedures     *
 *****************************************)
{$IFDEF MSWINDOWS}
// converts TComEvents type to Integer
function EventsToInt(const Events: TComEvents): Integer;
begin
  Result := 0;
  if evRxChar in Events then
    Result := Result or EV_RXCHAR;
  if evRing in Events then
    Result := Result or EV_RING;
  if evCTS in Events then
    Result := Result or EV_CTS;
  if evDSR in Events then
    Result := Result or EV_DSR;
  if evRLSD in Events then
    Result := Result or EV_RLSD;
  if evError in Events then
    Result := Result or EV_ERR;
end;

function IntToEvents(Mask: Integer): TComEvents;
begin
  Result := [];
  if (EV_RXCHAR and Mask) <> 0 then
    Result := Result + [evRxChar];
  if (EV_RING and Mask) <> 0 then
    Result := Result + [evRing];
  if (EV_CTS and Mask) <> 0 then
    Result := Result + [evCTS];
  if (EV_DSR and Mask) <> 0 then
    Result := Result + [evDSR];
  if (EV_RLSD and Mask) <> 0 then
    Result := Result + [evRLSD];
  if (EV_ERR and Mask) <> 0 then
    Result := Result + [evError];
end;
{$ENDIF}

(*****************************************
 * TComThread class                      *
 *****************************************)

// create thread
constructor TComThread.Create(AComPort: TCustomComPort);
begin
  inherited Create(False);
  FComPort := AComPort;
  {$IFDEF MSWINDOWS}
  FStopEvent := CreateEvent(nil, True, False, nil);
  SetCommMask(FComPort.Handle, EventsToInt(FComPort.Events));
  {$ELSE}
  FStop := false;
  {$ENDIF}
end;

// destroy thread
destructor TComThread.Destroy;
begin
  Stop;
  inherited Destroy;
end;

// thread action
{$IFDEF MSWINDOWS}
procedure TComThread.Execute;
var
  EventHandles: array[0..1] of THandle;
  Overlapped: TOverlapped;
  Signaled, BytesTrans, Mask: DWORD;
begin
  try
    FillChar(Overlapped, SizeOf(Overlapped), 0);
    Overlapped.hEvent := CreateEvent(nil, True, True, nil);
    EventHandles[0] := FStopEvent;
    EventHandles[1] := Overlapped.hEvent;
    repeat
      // wait for event to occur on serial port
      WaitCommEvent(FComPort.Handle, Mask, @Overlapped);
      Signaled := WaitForMultipleObjects(2, @EventHandles, False, INFINITE);
      // if event occurs, dispatch it
      if (Signaled = WAIT_OBJECT_0 + 1)
        and GetOverlappedResult(FComPort.Handle, Overlapped, BytesTrans, False)
      then
      begin
        FEvents := IntToEvents(Mask);
        DispatchComMsg;
      end;
    until Signaled <> (WAIT_OBJECT_0 + 1);
    // clear buffers
    SetCommMask(FComPort.Handle, 0);
    PurgeComm(FComPort.Handle, PURGE_TXCLEAR or PURGE_RXCLEAR);
    CloseHandle(Overlapped.hEvent);
    CloseHandle(FStopEvent);
  finally
    Terminate;
  end;
end;
{$ELSE}
procedure TComThread.Execute;
var Sig: TComSignals;
    bCTS,
    bDSR,
    bRing,
    bRLSD: boolean;

begin
  Sig := FComPort.Signals;
  bCTS := (csCTS in Sig);
  bDSR := (csDSR in Sig);
  bRing := (csRing in Sig);
  bRLSD := (csRLSD in Sig);
  try
    while not FStop do
    begin
      FEvents := [];

      if ( evRxChar in FComPort.Events ) and FComPort.CanRead(100) then
        FEvents := FEvents + [evRxChar];

      Sig := FComPort.Signals;

      if ( evCTS in FComPort.Events ) then
      begin
        if ((csCTS in Sig) and (not bCTS)) or
           (bCTS and (not (csCTS in Sig))) then
        FEvents := FEvents + [evCTS];
      end;

      if ( evDSR in FComPort.Events ) then
      begin
        if ((csDSR in Sig) and (not bDSR)) or
           (bDSR and (not (csDSR in Sig))) then
        FEvents := FEvents + [evDSR];
      end;

      if ( evRLSD in FComPort.Events ) then
      begin
        if ((csRLSD in Sig) and (not bRLSD)) or
           (bRLSD and (not (csRLSD in Sig))) then
        FEvents := FEvents + [evRLSD];
      end;

      if ( evRing in FComPort.Events ) and (csRing in Sig) and (not bRing) then
        FEvents := FEvents + [evRing];

      bCTS := (csCTS in Sig);
      bDSR := (csDSR in Sig);
      bRing := (csRing in Sig);
      bRLSD := (csRLSD in Sig);

      if (FEvents <> []) then DispatchComMsg else sleep(10);
    end;
  finally
    Terminate;
  end;
end;
{$ENDIF}

// stop thread
procedure TComThread.Stop;
begin
  {$IFDEF MSWINDOWS}
  SetEvent(FStopEvent);
  Sleep(0);
  {$ELSE}
  FStop := true;
  {$ENDIF}
end;

// dispatch events
procedure TComThread.DispatchComMsg;
begin
  Synchronize(DoEvents); // call events in main thread
end;

// call events
procedure TComThread.DoEvents;
begin
  if evRxChar in FEvents then
    FComPort.CallRxChar;
  if evRing in FEvents then
    FComPort.CallRing;
  if evCTS in FEvents then
    FComPort.CallCTSChange;
  if evDSR in FEvents then
    FComPort.CallDSRChange;
  if evRLSD in FEvents then
    FComPort.CallRLSDChange;
  if evError in FEvents then
    FComPort.CallError;
end;

{$IFDEF MSWINDOWS}
(*****************************************
 * TComTimeouts class                    *
 *****************************************)

// create class
constructor TComTimeouts.Create;
begin
  inherited Create;
  FReadInterval := -1;
  FWriteTotalM := 100;
  FWriteTotalC := 1000;
end;

// copy properties to other class
procedure TComTimeouts.AssignTo(Dest: TPersistent);
begin
  if Dest is TComTimeouts then
  begin
    with TComTimeouts(Dest) do
    begin
      FReadInterval := Self.ReadInterval;
      FReadTotalM   := Self.ReadTotalMultiplier;
      FReadTotalC   := Self.ReadTotalConstant;
      FWriteTotalM  := Self.WriteTotalMultiplier;
      FWriteTotalC  := Self.WriteTotalConstant;
    end
  end
  else
    inherited AssignTo(Dest);
end;

// select TCustomComPort to own this class
procedure TComTimeouts.SetComPort(const AComPort: TCustomComPort);
begin
  FComPort := AComPort;
end;

// set read interval
procedure TComTimeouts.SetReadInterval(const Value: Integer);
begin
  if Value <> FReadInterval then
  begin
    FReadInterval := Value;
    // if possible, apply the changes
    if FComPort <> nil then
      FComPort.ApplyTimeouts;
  end;
end;

// set read total constant
procedure TComTimeouts.SetReadTotalC(const Value: Integer);
begin
  if Value <> FReadTotalC then
  begin
    FReadTotalC := Value;
    if FComPort <> nil then
      FComPort.ApplyTimeouts;
  end;
end;

// set read total multiplier
procedure TComTimeouts.SetReadTotalM(const Value: Integer);
begin
  if Value <> FReadTotalM then
  begin
    FReadTotalM := Value;
    if FComPort <> nil then
      FComPort.ApplyTimeouts;
  end;
end;

// set write total constant
procedure TComTimeouts.SetWriteTotalC(const Value: Integer);
begin
  if Value <> FWriteTotalC then
  begin
    FWriteTotalC := Value;
    if FComPort <> nil then
      FComPort.ApplyTimeouts;
  end;
end;

// set write total multiplier
procedure TComTimeouts.SetWriteTotalM(const Value: Integer);
begin
  if Value <> FWriteTotalM then
  begin
    FWriteTotalM := Value;
    if FComPort <> nil then
      FComPort.ApplyTimeouts;
  end;
end;

{$ENDIF}

(*****************************************
 * TComFlowControl class                 *
 *****************************************)

// create class
constructor TComFlowControl.Create;
begin
  inherited Create;
  FXonChar := #17;
  FXoffChar := #19;
end;

// copy properties to other class
procedure TComFlowControl.AssignTo(Dest: TPersistent);
begin
  if Dest is TComFlowControl then
  begin
    with TComFlowControl(Dest) do
    begin
      FOutCTSFlow       := Self.OutCTSFlow;
      FOutDSRFlow       := Self.OutDSRFlow;
      FControlDTR       := Self.ControlDTR;
      FControlRTS       := Self.ControlRTS;
      FXonXoffOut       := Self.XonXoffOut;
      FXonXoffIn        := Self.XonXoffIn;
      FTxContinueOnXoff := Self.TxContinueOnXoff;
      FDSRSensitivity   := Self.DSRSensitivity;
      FXonChar          := Self.XonChar;
      FXoffChar         := Self.XoffChar;
    end
  end
  else
    inherited AssignTo(Dest);
end;

// select TCustomComPort to own this class
procedure TComFlowControl.SetComPort(const AComPort: TCustomComPort);
begin
  FComPort := AComPort;
end;

// set input flow control for DTR (data-terminal-ready)
procedure TComFlowControl.SetControlDTR(const Value: TDTRFlowControl);
begin
  if Value <> FControlDTR then
  begin
    FControlDTR := Value;
    if FComPort <> nil then
      FComPort.ApplyDCB;
  end;
end;

// set input flow control for RTS (request-to-send)
procedure TComFlowControl.SetControlRTS(const Value: TRTSFlowControl);
begin
  if Value <> FControlRTS then
  begin
    FControlRTS := Value;
    if FComPort <> nil then
      FComPort.ApplyDCB;
  end;
end;

// set ouput flow control for CTS (clear-to-send)
procedure TComFlowControl.SetOutCTSFlow(const Value: Boolean);
begin
  if Value <> FOutCTSFlow then
  begin
    FOutCTSFlow := Value;
    if FComPort <> nil then
      FComPort.ApplyDCB;
  end;
end;

// set output flow control for DSR (data-set-ready)
procedure TComFlowControl.SetOutDSRFlow(const Value: Boolean);
begin
  if Value <> FOutDSRFlow then
  begin
    FOutDSRFlow := Value;
    if FComPort <> nil then
      FComPort.ApplyDCB;
  end;
end;

// set software input flow control
procedure TComFlowControl.SetXonXoffIn(const Value: Boolean);
begin
  if Value <> FXonXoffIn then
  begin
    FXonXoffIn := Value;
    if FComPort <> nil then
      FComPort.ApplyDCB;
  end;
end;

// set software ouput flow control
procedure TComFlowControl.SetXonXoffOut(const Value: Boolean);
begin
  if Value <> FXonXoffOut then
  begin
    FXonXoffOut := Value;
    if FComPort <> nil then
      FComPort.ApplyDCB;
  end;
end;

// set DSR sensitivity
procedure TComFlowControl.SetDSRSensitivity(const Value: Boolean);
begin
  if Value <> FDSRSensitivity then
  begin
    FDSRSensitivity := Value;
    if FComPort <> nil then
      FComPort.ApplyDCB;
  end;
end;

// set transfer continue when Xoff is sent
procedure TComFlowControl.SetTxContinueOnXoff(const Value: Boolean);
begin
  if Value <> FTxContinueOnXoff then
  begin
    FTxContinueOnXoff := Value;
    if FComPort <> nil then
      FComPort.ApplyDCB;
  end;
end;

// set Xon char
procedure TComFlowControl.SetXonChar(const Value: AnsiChar);
begin
  if Value <> FXonChar then
  begin
    FXonChar := Value;
    if FComPort <> nil then
      FComPort.ApplyDCB;
  end;
end;

// set Xoff char
procedure TComFlowControl.SetXoffChar(const Value: AnsiChar);
begin
  if Value <> FXoffChar then
  begin
    FXoffChar := Value;
    if FComPort <> nil then
      FComPort.ApplyDCB;
  end;
end;

// get common flow control
function TComFlowControl.GetFlowControl: TFlowControl;
begin
  if (FControlRTS = rtsHandshake) and (FOutCTSFlow)
    and (not FXonXoffIn) and (not FXonXoffOut)
  then
    Result := fcHardware
  else
    if (FControlRTS = rtsDisable) and (not FOutCTSFlow)
      and (FXonXoffIn) and (FXonXoffOut)
    then
      Result := fcSoftware
    else
      if (FControlRTS = rtsDisable) and (not FOutCTSFlow)
        and (not FXonXoffIn) and (not FXonXoffOut)
      then
        Result := fcNone
      else
        Result := fcCustom;
end;

// set common flow control
procedure TComFlowControl.SetFlowControl(const Value: TFlowControl);
begin
  if Value <> fcCustom then
  begin
    FControlRTS := rtsDisable;
    FOutCTSFlow := False;
    FXonXoffIn := False;
    FXonXoffOut := False;
    case Value of
      fcHardware:
      begin
        FControlRTS := rtsHandshake;
        FOutCTSFlow := True;
      end;
      fcSoftware:
      begin
        FXonXoffIn := True;
        FXonXoffOut := True;
      end;
    end;
  end;
  if FComPort <> nil then
    FComPort.ApplyDCB;
end;

(*****************************************
 * TComParity class                      *
 *****************************************)

// create class
constructor TComParity.Create;
begin
  inherited Create;
  FBits := prNone;
end;

// copy properties to other class
procedure TComParity.AssignTo(Dest: TPersistent);
begin
  if Dest is TComParity then
  begin
    with TComParity(Dest) do
    begin
      FBits        := Self.Bits;
      FCheck       := Self.Check;
      FReplace     := Self.Replace;
      FReplaceChar := Self.ReplaceChar;
    end
  end
  else
    inherited AssignTo(Dest);
end;

// select TCustomComPort to own this class
procedure TComParity.SetComPort(const AComPort: TCustomComPort);
begin
  FComPort := AComPort;
end;

// set parity bits
procedure TComParity.SetBits(const Value: TParityBits);
begin
  if Value <> FBits then
  begin
    FBits := Value;
    if FComPort <> nil then
      FComPort.ApplyDCB;
  end;
end;

// set check parity
procedure TComParity.SetCheck(const Value: Boolean);
begin
  if Value <> FCheck then
  begin
    FCheck := Value;
    if FComPort <> nil then
      FComPort.ApplyDCB;
  end;
end;

// set replace on parity error
procedure TComParity.SetReplace(const Value: Boolean);
begin
  if Value <> FReplace then
  begin
    FReplace := Value;
    if FComPort <> nil then
      FComPort.ApplyDCB;
  end;
end;

// set replace char
procedure TComParity.SetReplaceChar(const Value: AnsiChar);
begin
  if Value <> FReplaceChar then
  begin
    FReplaceChar := Value;
    if FComPort <> nil then
      FComPort.ApplyDCB;
  end;
end;

(*****************************************
 * TComBuffer class                      *
 *****************************************)

// create class
constructor TComBuffer.Create;
begin
  inherited Create;
  FInputSize := 2048;
  FOutputSize := 2048;
end;

// copy properties to other class
procedure TComBuffer.AssignTo(Dest: TPersistent);
begin
  if Dest is TComBuffer then
  begin
    with TComBuffer(Dest) do
    begin
      FOutputSize  := Self.OutputSize;
      FInputSize   := Self.InputSize;
    end
  end
  else
    inherited AssignTo(Dest);
end;

// select TCustomComPort to own this class
procedure TComBuffer.SetComPort(const AComPort: TCustomComPort);
begin
  FComPort := AComPort;
end;

// set input size
procedure TComBuffer.SetInputSize(const Value: Integer);
begin
  if Value <> FInputSize then
  begin
    FInputSize := Value;
    if (FInputSize mod 2) = 1 then
      Dec(FInputSize);
    if FComPort <> nil then
      FComPort.ApplyBuffer;
  end;
end;

// set ouput size
procedure TComBuffer.SetOutputSize(const Value: Integer);
begin
  if Value <> FOutputSize then
  begin
    FOutputSize := Value;
    if (FOutputSize mod 2) = 1 then
      Dec(FOutputSize);
    if FComPort <> nil then
      FComPort.ApplyBuffer;
  end;
end;

(*****************************************
 * TCustomComPort component              *
 *****************************************)

// create component
constructor TCustomComPort.Create(AOwner: TComponent);
var Ports: TStringList;
begin
  inherited Create(AOwner);
  // component cannot reside on inheritable forms
  FLastError := sOK;
  FLastErrorDesc := '';
  FRaiseExcept := false;
  FComponentStyle := FComponentStyle - [csInheritable];
  FTriggersOnRxChar := True;
  FBaudRate := br9600;
  FCustomBaudRate := 9600;
  FPort := 'None';
  Ports:=TStringList.Create;
  try
    EnumComPorts(Ports);
    if Ports.Count>0 then
    FPort := Ports.Strings[0];
  finally
    Ports.Free;
  end;
  FStopBits := sbOneStopBit;
  FDataBits := dbEight;
  FEvents := [evRxChar, evRing, evCTS, evDSR, evRLSD, evError];
  FHandle := INVALID_HANDLE_VALUE;
  FStoredProps := [spBasic];
  SetSynaError(sOK);
  FParity := TComParity.Create;
  FParity.SetComPort(Self);
  FFlowControl := TComFlowControl.Create;
  FFlowControl.SetComPort(Self);
  {$IFDEF MSWINDOWS}
  FTimeouts := TComTimeouts.Create;
  FTimeouts.SetComPort(Self);
  {$ENDIF}
  FBuffer := TComBuffer.Create;
  FBuffer.SetComPort(Self);
end;

// destroy component
destructor TCustomComPort.Destroy;
begin
  Close;
  FBuffer.Free;
  FFlowControl.Free;
  {$IFDEF MSWINDOWS}
  FTimeouts.Free;
  {$ENDIF}
  FParity.Free;
  inherited Destroy;
end;

// create handle to serial port
procedure TCustomComPort.CreateHandle;
begin
{$IFDEF MSWINDOWS}
  SetLastError(sOK);
  FHandle := CreateFile(
    PChar('\\.\' + FPort),
    GENERIC_READ or GENERIC_WRITE,
    0,
    nil,
    OPEN_EXISTING,
    FILE_FLAG_OVERLAPPED,
    0);
{$ELSE}
  {$IFNDEF FPC}
  SetLastError(sOK);
  FHandle := THandle(Libc.open(pchar(FPort), O_RDWR or O_SYNC));
  {$ELSE}
  fpSetErrno(sOK);
  FHandle := THandle(fpOpen(FPort, O_RDWR or O_SYNC));
  {$ENDIF}
{$ENDIF}

  if (FHandle = INVALID_HANDLE_VALUE) then  //because THandle is not integer on all platforms!
    SerialCheck(-1)
  else
    SerialCheck(0);

  ExceptCheck;
  if FLastError <> sOK then Exit;

  SetSynaError(sOK);
  ExceptCheck;
end;

// destroy serial port handle
procedure TCustomComPort.DestroyHandle;
begin
  if FHandle <> INVALID_HANDLE_VALUE then
  begin
    ClearBuffers;
    {$IFDEF MSWINDOWS}
    CloseHandle(FHandle);
    {$ELSE}
    FileClose(FHandle);
    {$ENDIF}
  end;
  FHandle := INVALID_HANDLE_VALUE;
  SetSynaError(sOK);
end;

procedure TCustomComPort.Loaded;
begin
  inherited Loaded;
  // open port if Connected is True at design-time
  if FConnected then
  begin
    FConnected := False;
    try
      Open;
    except
      Application.HandleException(Self);
    end;
  end;
end;

// prevent from applying changes at runtime
procedure TCustomComPort.BeginUpdate;
begin
  FUpdateCount := FUpdateCount + 1;
end;

// apply the changes made since BeginUpdate call
procedure TCustomComPort.EndUpdate;
begin
  if FUpdateCount > 0 then
  begin
    FUpdateCount := FUpdateCount - 1;
    if FUpdateCount = 0 then
      SetupComPort;
  end;
end;

// open port
procedure TCustomComPort.Open;
begin
  // if already connected, do nothing
  if not FConnected then
  begin
    CallBeforeOpen;

    {$IFDEF MSWINDOWS}
    SetLastError (sOK);
    {$ELSE}
      {$IFNDEF FPC}
      SetLastError (sOK);
      {$ELSE}
      fpSetErrno(sOK);
      {$ENDIF}
    {$ENDIF}

    // open port
    CreateHandle;
    FConnected := True;
    try
      // initialize port
      SetupComPort;
    except
      // error occured during initialization, destroy handle
      DestroyHandle;
      FConnected := False;
      raise;
    end;
    // if at least one event is set, create special thread to monitor port
    if (FEvents = []) then
      FThreadCreated := False
    else
    begin
      FEventThread := TComThread.Create(Self);
      FThreadCreated := True;
    end;
    // port is succesfully opened, do any additional initialization
    CallAfterOpen;
  end;
end;

// close port
procedure TCustomComPort.Close;
begin
  // if already closed, do nothing
  if FConnected then
  begin
    CallBeforeClose;
    {$IFDEF MSWINDOWS}
    // abort all pending operations
    AbortAllAsync;
    {$ENDIF}
    // stop monitoring for events
    if FThreadCreated then
    begin
      FEventThread.Free;
      FThreadCreated := False;
    end;
    // close port
    DestroyHandle;
    FConnected := False;
    // port is closed, do any additional finalization
    CallAfterClose;
  end;
end;

{$IFNDEF MSWINDOWS}
procedure TCustomComPort.TermiosToDcb(const term: termios; var dcb: TDCB);
var
  n: integer;
  x: cardinal;
begin
  dcb.baudrate := 0;
 {$IFDEF FPC}
  x := term.c_oflag and $0F;
 {$ELSE}
  x := cfgetospeed(term);
 {$ENDIF}
  for n := 0 to Maxrates do
    if rates[n, 1] = x then
    begin
      dcb.baudrate := rates[n, 0];
      break;
    end;
  //hardware handshake
  if (term.c_cflag and CRTSCTS) > 0 then
    dcb.flags := dcb.flags or dcb_RtsControlHandshake or dcb_OutxCtsFlow
  else
    dcb.flags := dcb.flags and (not (dcb_RtsControlHandshake or dcb_OutxCtsFlow));
  //software handshake
  if (term.c_cflag and IXOFF) > 0 then
    dcb.flags := dcb.flags or dcb_OutX or dcb_InX
  else
    dcb.flags := dcb.flags and (not (dcb_OutX or dcb_InX));
  //size of byte
  case term.c_cflag and CSIZE of
    CS5:
      dcb.bytesize := 5;
    CS6:
      dcb.bytesize := 6;
    CS7fix:
      dcb.bytesize := 7;
    CS8:
      dcb.bytesize := 8;
  end;
  //parity
  if (term.c_cflag and PARENB) > 0 then
    dcb.flags := dcb.flags or dcb_Parity
  else
    dcb.flags := dcb.flags and (not dcb_Parity);
  dcb.parity := 0;
  if (term.c_cflag and PARODD) > 0 then
    dcb.parity := 1
  else
    dcb.parity := 2;
  //stop bits
  if (term.c_cflag and CSTOPB) > 0 then
    dcb.stopbits := 2
  else
    dcb.stopbits := 0;
end;

procedure TCustomComPort.DcbToTermios(const dcb: TDCB; var term: termios);
var
  n: integer;
  x: cardinal;
begin
  cfmakeraw(term);
  term.c_cflag := term.c_cflag or CREAD;
  term.c_cflag := term.c_cflag or CLOCAL;
  term.c_cflag := term.c_cflag or HUPCL;
  //hardware handshake
  if (dcb.flags and dcb_RtsControlHandshake) > 0 then
    term.c_cflag := term.c_cflag or CRTSCTS
  else
    term.c_cflag := term.c_cflag and (not CRTSCTS);
  //software handshake
  if (dcb.flags and dcb_OutX) > 0 then
    term.c_iflag := term.c_iflag or IXON or IXOFF or IXANY
  else
    term.c_iflag := term.c_iflag and (not (IXON or IXOFF or IXANY));
  //size of byte
  term.c_cflag := term.c_cflag and (not CSIZE);
  case dcb.bytesize of
    5:
      term.c_cflag := term.c_cflag or CS5;
    6:
      term.c_cflag := term.c_cflag or CS6;
    7:
{$IFDEF FPC}
      term.c_cflag := term.c_cflag or CS7;
{$ELSE}
      term.c_cflag := term.c_cflag or CS7fix;
{$ENDIF}
    8:
      term.c_cflag := term.c_cflag or CS8;
  end;
  //parity
  if (dcb.flags and dcb_Parity) > 0 then
    term.c_cflag := term.c_cflag or PARENB
  else
    term.c_cflag := term.c_cflag and (not PARENB);
  case dcb.parity of
    1: //'O'
      term.c_cflag := term.c_cflag or PARODD;
    2: //'E'
      term.c_cflag := term.c_cflag and (not PARODD);
  end;
  //stop bits
  if dcb.stopbits > 0 then
    term.c_cflag := term.c_cflag or CSTOPB
  else
    term.c_cflag := term.c_cflag and (not CSTOPB);
  //set baudrate;
  x := 0;
  for n := 0 to Maxrates do
    if rates[n, 0] = dcb.BaudRate then
    begin
      x := rates[n, 1];
      break;
    end;
  cfsetospeed(term, x);
  cfsetispeed(term, x);
end;

function TCustomComPort.CanRead(TimeOut: integer): boolean;
var
  FDSet: TFDSet;
  TimeVal: PTimeVal;
  TimeV: TTimeVal;
  x: Integer;
begin
  TimeV.tv_usec := (Timeout mod 1000) * 1000;
  TimeV.tv_sec := Timeout div 1000;
  TimeVal := @TimeV;
  if Timeout = -1 then
    TimeVal := nil;
  {$IFNDEF FPC}
  FD_ZERO(FDSet);
  FD_SET(FHandle, FDSet);
  x := Select(FHandle + 1, @FDSet, nil, nil, TimeVal);
  {$ELSE}
  fpFD_ZERO(FDSet);
  fpFD_SET(FHandle, FDSet);
  x := fpSelect(FHandle + 1, @FDSet, nil, nil, TimeVal);
  {$ENDIF}
  SerialCheck(x);
  if FLastError <> sOK then x := 0;
  Result := x > 0;
  ExceptCheck;
end;
{$ENDIF}

// apply port properties
procedure TCustomComPort.ApplyDCB;
const
  CParityBits: array[TParityBits] of Integer =
    (0, 1, 2, 3, 4);
  CStopBits: array[TStopBits] of Integer =
    (0, 1, 2);
  CBaudRate: array[TBaudRate] of Integer =
    (0, 110, 300, 600, 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200);
  CDataBits: array[TDataBits] of Integer = (5, 6, 7, 8);
  CControlRTS: array[TRTSFlowControl] of Integer =
    (RTS_CONTROL_DISABLE shl 12,
     RTS_CONTROL_ENABLE shl 12,
     RTS_CONTROL_HANDSHAKE shl 12,
     RTS_CONTROL_TOGGLE shl 12);
  CControlDTR: array[TDTRFlowControl] of Integer =
    (DTR_CONTROL_DISABLE shl 4,
     DTR_CONTROL_ENABLE shl 4,
     DTR_CONTROL_HANDSHAKE shl 4);

var
  DCB: TDCB;
  {$IFNDEF MSWINDOWS}
  TermiosStruc: termios;
  {$ENDIF}
begin
  // if not connected or inside BeginUpdate/EndUpdate block, do nothing
  if FConnected and (FUpdateCount = 0) then
  begin
    FillChar(DCB, SizeOf(TDCB), 0);
    {$IFDEF MSWINDOWS}
    SetSynaError(sOK);
    if not windows.GetCommState(FHandle, DCB) then
      SerialCheck( sErr );
    ExceptCheck;
    {$ELSE}
    SerialCheck(TCGetAttr(FHandle, TermiosStruc));
    ExceptCheck;
    TermiosToDCB(TermiosStruc, DCB);
    {$ENDIF}

    DCB.DCBlength := SizeOf(TDCB);
    DCB.XonLim := FBuffer.InputSize div 4;
    DCB.XoffLim := DCB.XonLim;

    DCB.Flags := dcb_Binary;
    if FDiscardNull then
      DCB.Flags := DCB.Flags or dcb_Null;

    with FFlowControl do
    begin
      DCB.XonChar := XonChar;
      DCB.XoffChar := XoffChar;
      if OutCTSFlow then
        DCB.Flags := DCB.Flags or dcb_OutxCTSFlow;
      if OutDSRFlow then
        DCB.Flags := DCB.Flags or dcb_OutxDSRFlow;
      DCB.Flags := DCB.Flags or CControlDTR[ControlDTR]
        or CControlRTS[ControlRTS];
      if XonXoffOut then
        DCB.Flags := DCB.Flags or dcb_OutX;
      if XonXoffIn then
        DCB.Flags := DCB.Flags or dcb_InX;
      if DSRSensitivity then
        DCB.Flags := DCB.Flags or dcb_DSRSensivity;
      if TxContinueOnXoff then
        DCB.Flags := DCB.Flags or dcb_TxContinueOnXoff;
    end;

    DCB.Parity := CParityBits[FParity.Bits];
    DCB.StopBits := CStopBits[FStopBits];
    if FBaudRate <> brCustom then
      DCB.BaudRate := CBaudRate[FBaudRate]
    else
      DCB.BaudRate := FCustomBaudRate;
    DCB.ByteSize := CDataBits[FDataBits];

    if FParity.Check then
    begin
      DCB.Flags := DCB.Flags or dcb_Parity;
      if FParity.Replace then
      begin
        DCB.Flags := DCB.Flags or dcb_ErrorChar;
        DCB.ErrorChar := FParity.ReplaceChar;
      end;
    end;

    {$IFDEF MSWINDOWS}
    SetSynaError(sOK);
    if not windows.SetCommState(FHandle, DCB) then
      SerialCheck(sErr);
    {$ELSE}
    DcbToTermios(dcb, termiosstruc);
    SerialCheck(tcsetattr(FHandle, TCSANOW, termiosstruc));
    {$ENDIF}
    ExceptCheck;
  end;
end;

{$IFDEF MSWINDOWS}
// apply timeout properties
procedure TCustomComPort.ApplyTimeouts;
var
  Timeouts: TCommTimeouts;

  function GetTOValue(const Value: Integer): DWORD;
  begin
    if Value = -1 then
      Result := MAXDWORD
    else
      Result := Value;
  end;

begin
  if FConnected and (FUpdateCount = 0) then
  begin
    Timeouts.ReadIntervalTimeout := GetTOValue(FTimeouts.ReadInterval);
    Timeouts.ReadTotalTimeoutMultiplier := GetTOValue(FTimeouts.ReadTotalMultiplier);
    Timeouts.ReadTotalTimeoutConstant := GetTOValue(FTimeouts.ReadTotalConstant);
    Timeouts.WriteTotalTimeoutMultiplier := GetTOValue(FTimeouts.WriteTotalMultiplier);
    Timeouts.WriteTotalTimeoutConstant := GetTOValue(FTimeouts.WriteTotalConstant);

    // apply settings
    SetSynaError(sOK);
    if not SetCommTimeouts(FHandle, Timeouts) then
      SerialCheck(sErr);
    ExceptCheck;
  end;
end;
{$ENDIF}

// apply buffers
procedure TCustomComPort.ApplyBuffer;
begin
  // if not connected or inside BeginUpdate/EndUpdate block, do nothing
  if FConnected and (FUpdateCount = 0) then
  begin
    {$IFDEF MSWINDOWS}
    //apply settings
    SetSynaError(sOK);
    if not SetupComm(FHandle, FBuffer.InputSize, FBuffer.OutputSize) then
      SerialCheck(sErr);
    ExceptCheck;
    {$ELSE}

    {$ENDIF}
  end;
end;

// initialize port
procedure TCustomComPort.SetupComPort;
begin
  ApplyBuffer;
  ApplyDCB;
  {$IFDEF MSWINDOWS}
  ApplyTimeouts;
  {$ENDIF}
end;

// get number of bytes in input buffer
function TCustomComPort.InputCount: Integer;
{$IFDEF MSWINDOWS}
var
  Errors: DWORD;
  ComStat: TComStat;
begin
  Result := 0;
  SetSynaError(sOK);
  if not ClearCommError(FHandle, Errors, @ComStat) then
    SerialCheck(sErr);
  ExceptCheck;

  Result := ComStat.cbInQue;
{$ELSE}
begin
  {$IFNDEF FPC}
  SerialCheck(ioctl(FHandle, FIONREAD, @result));
  {$ELSE}
  SerialCheck(fpIoctl(FHandle, FIONREAD, @result));
  {$ENDIF}
  if FLastError <> 0 then
    Result := 0;
  ExceptCheck;
{$ENDIF}
end;

// get number of bytes in output buffer
function TCustomComPort.OutputCount: Integer;
{$IFDEF MSWINDOWS}
var
  Errors: DWORD;
  ComStat: TComStat;
begin
  Result := 0;
  SetSynaError(sOK);
  if not ClearCommError(FHandle, Errors, @ComStat) then
    SerialCheck(sErr);
  ExceptCheck;
  Result := ComStat.cbOutQue;
{$ELSE}
begin
  Result:=0;
{$ENDIF}
end;

// get signals which are in high state
function TCustomComPort.Signals: TComSignals;
var
  Status: DWORD;
begin
  Result := [];

{$IFDEF MSWINDOWS}
  SetSynaError(sOK);
  if not GetCommModemStatus(FHandle, Status) then
    SerialCheck(sErr);
  ExceptCheck;
  if (MS_CTS_ON and Status) <> 0 then
    Result := Result + [csCTS];
  if (MS_DSR_ON and Status) <> 0 then
    Result := Result + [csDSR];
  if (MS_RING_ON and Status) <> 0 then
    Result := Result + [csRing];
  if (MS_RLSD_ON and Status) <> 0 then
    Result := Result + [csRLSD];
{$ELSE}
  {$IFNDEF FPC}
  SerialCheck(ioctl(FHandle, TIOCMGET, @Status));
  {$ELSE}
  SerialCheck(fpioctl(FHandle, TIOCMGET, @Status));
  {$ENDIF}
  ExceptCheck;
  if (TIOCM_CTS and Status) <> 0 then
    Result := Result + [csCTS];
  if (TIOCM_DSR and Status) <> 0 then
    Result := Result + [csDSR];
  if (TIOCM_RNG and Status) <> 0 then
    Result := Result + [csRing];
  if (TIOCM_CAR and Status) <> 0 then
    Result := Result + [csRLSD];
{$ENDIF}
end;

// set hardware line break
procedure TCustomComPort.SetBreak(Duration: Integer);
{$IFDEF MSWINDOWS}
var
  Act: Integer;
begin
  Act := Windows.SETBREAK;
  SetSynaError(sOK);
  if not EscapeCommFunction(FHandle, Act) then
    SerialCheck(sErr);
  ExceptCheck;
  Sleep(Duration);
  Act := Windows.CLRBREAK;
  if not EscapeCommFunction(FHandle, Act) then
    SerialCheck(sErr);
  ExceptCheck;
{$ELSE}
begin
  SerialCheck(tcsendbreak(FHandle, Duration));
  ExceptCheck;
{$ENDIF}
end;

// set DTR signal
procedure TCustomComPort.SetDTR(OnOff: Boolean);
var
  Act: DWORD;
begin
{$IFDEF MSWINDOWS}
  if OnOff then
    Act := Windows.SETDTR
  else
    Act := Windows.CLRDTR;
  SetSynaError(sOK);
  if not EscapeCommFunction(FHandle, Act) then
    SerialCheck(sErr);
  ExceptCheck;
{$ELSE}
  {$IFNDEF FPC}
  SerialCheck(ioctl(FHandle, TIOCMGET, @Act));
  {$ELSE}
  SerialCheck(fpioctl(FHandle, TIOCMGET, @Act));
  {$ENDIF}
  ExceptCheck;
  if OnOff then
    Act := Act or TIOCM_DTR
  else
    Act := Act and not TIOCM_DTR;
  {$IFNDEF FPC}
  SerialCheck(ioctl(FHandle, TIOCMSET, @Act));
  {$ELSE}
  SerialCheck(fpioctl(FHandle, TIOCMSET, @Act));
  {$ENDIF}
  ExceptCheck;
{$ENDIF}
end;

// set RTS signals
procedure TCustomComPort.SetRTS(OnOff: Boolean);
var
  Act: DWORD;
begin
{$IFDEF MSWINDOWS}
  if OnOff then
    Act := Windows.SETRTS
  else
    Act := Windows.CLRRTS;
  SetSynaError(sOK);
  if not EscapeCommFunction(FHandle, Act) then
    SerialCheck(sErr);
  ExceptCheck;
{$ELSE}
  {$IFNDEF FPC}
  SerialCheck(ioctl(FHandle, TIOCMGET, @Act));
  {$ELSE}
  SerialCheck(fpioctl(FHandle, TIOCMGET, @Act));
  {$ENDIF}
  ExceptCheck;
  if OnOff then
    Act := Act or TIOCM_RTS
  else
    Act := Act and not TIOCM_RTS;
  {$IFNDEF FPC}
  SerialCheck(ioctl(FHandle, TIOCMSET, @Act));
  {$ELSE}
  SerialCheck(fpioctl(FHandle, TIOCMSET, @Act));
  {$ENDIF}
  ExceptCheck;
{$ENDIF}
end;

// clear input and output buffers
procedure TCustomComPort.ClearBuffers;
begin
{$IFDEF MSWINDOWS}
  SetSynaError(sOK);
  if not PurgeComm(FHandle, PURGE_TXABORT or PURGE_TXCLEAR or PURGE_RXABORT or PURGE_RXCLEAR) then
    SerialCheck(sErr);
  ExceptCheck;
{$ELSE}
  {$IFNDEF FPC}
  SerialCheck(ioctl(FHandle, TCFLSH, TCIOFLUSH));
  {$ELSE}
  SerialCheck(fpioctl(FHandle, TCFLSH, Pointer(PtrInt(TCIOFLUSH))));
  {$ENDIF}
  ExceptCheck;
{$ENDIF}
end;

// clear input buffer
procedure TCustomComPort.ClearInputBuffer;
begin
{$IFDEF MSWINDOWS}
  SetSynaError(sOK);
  if not PurgeComm(FHandle, PURGE_RXABORT or PURGE_RXCLEAR) then
    SerialCheck(sErr);
  ExceptCheck;
{$ELSE}
  {$IFNDEF FPC}
  SerialCheck(ioctl(FHandle, TCFLSH, TCIFLUSH));
  {$ELSE}
  SerialCheck(fpioctl(FHandle, TCFLSH, Pointer(PtrInt(TCIFLUSH))));
  {$ENDIF}
  ExceptCheck;
{$ENDIF}
end;

// clear output buffer
procedure TCustomComPort.ClearOutputBuffer;
begin
{$IFDEF MSWINDOWS}
  SetSynaError(sOK);
  if not PurgeComm(FHandle, PURGE_TXABORT or PURGE_TXCLEAR) then
    SerialCheck(sErr);
  ExceptCheck;
{$ELSE}
  {$IFNDEF FPC}
  SerialCheck(ioctl(FHandle, TCFLSH, TCOFLUSH));
  {$ELSE}
  SerialCheck(fpioctl(FHandle, TCFLSH, Pointer(PtrInt(TCOFLUSH))));
  {$ENDIF}
  ExceptCheck;
{$ENDIF}
end;

{$IFDEF MSWINDOWS}
// prepare PAsync variable for read/write operation
procedure PrepareAsync(AKind: TOperationKind; const Buffer;
  Count: Integer; AsyncPtr: PAsync);
begin
  with AsyncPtr^ do
  begin
    Kind := AKind;
    if Data <> nil then
      FreeMem(Data);
    GetMem(Data, Count);
    Move(Buffer, Data^, Count);
    Size := Count;
  end;
end;

// perform asynchronous write operation
function TCustomComPort.WriteAsync(const Buffer; Count: Integer; var AsyncPtr: PAsync): Integer;
var
  Success: Boolean;
  BytesTrans: DWORD;
begin
  if AsyncPtr = nil then
     RaiseSynaError(ErrAsyncNil);

  PrepareAsync(okWrite, Buffer, Count, AsyncPtr);

  Success := WriteFile(FHandle, Buffer, Count, BytesTrans, @AsyncPtr^.Overlapped)
    or (GetLastError = ERROR_IO_PENDING);

  SetSynaError(sOK);
  if not Success then
    SerialCheck(sErr);
  ExceptCheck;

  Result := BytesTrans;
end;

// perform asynchronous write operation
function TCustomComPort.WriteStrAsync(const Str: AnsiString; var AsyncPtr: PAsync): Integer;
begin
  if Length(Str) > 0 then
    Result := WriteAsync(Str[1], Length(Str), AsyncPtr)
  else
    Result := 0;
end;

// perform asynchronous read operation
function TCustomComPort.ReadAsync(var Buffer; Count: Integer; var AsyncPtr: PAsync): Integer;
var
  Success: Boolean;
  BytesTrans: DWORD;
begin
  if AsyncPtr = nil then
    RaiseSynaError(ErrAsyncNil);
  AsyncPtr^.Kind := okRead;

  Success := ReadFile(FHandle, Buffer, Count, BytesTrans, @AsyncPtr^.Overlapped)
    or (GetLastError = ERROR_IO_PENDING);

  SetSynaError(sOK);
  if not Success then
    SerialCheck(sErr);
  ExceptCheck;

  Result := BytesTrans;
end;

// perform asynchronous read operation
function TCustomComPort.ReadStrAsync(var Str: AnsiString; Count: Integer; var AsyncPtr: PAsync): Integer;
begin
  SetLength(Str, Count);
  if Count > 0 then
    Result := ReadAsync(Str[1], Count, AsyncPtr)
  else
    Result := 0;
end;

// wait for asynchronous operation to end
function TCustomComPort.WaitForAsync(var AsyncPtr: PAsync): Integer;
var
  BytesTrans, Signaled: DWORD;
  Success: Boolean;
begin
  if AsyncPtr = nil then
    RaiseSynaError(ErrAsyncNil);

  Signaled := WaitForSingleObject(AsyncPtr^.Overlapped.hEvent, INFINITE);
  Success := (Signaled = WAIT_OBJECT_0) and
      (GetOverlappedResult(FHandle, AsyncPtr^.Overlapped, BytesTrans, False));

  SetSynaError(sOK);
  if not Success then
    SerialCheck(sErr);
  ExceptCheck;

  Result := BytesTrans;
end;

// abort all asynchronous operations
procedure TCustomComPort.AbortAllAsync;
begin
  SetSynaError(sOK);
  if not PurgeComm(FHandle, PURGE_TXABORT or PURGE_RXABORT) then
    SerialCheck(sErr);
  ExceptCheck;
end;

// detect whether asynchronous operation is completed
function TCustomComPort.IsAsyncCompleted(AsyncPtr: PAsync): Boolean;
var
  BytesTrans: DWORD;
begin
  if AsyncPtr = nil then
    RaiseSynaError(ErrAsyncNil);

  Result := GetOverlappedResult(FHandle, AsyncPtr^.Overlapped, BytesTrans, False);
  SetSynaError(sOK);
  if not Result then
    if (GetLastError <> ERROR_IO_PENDING) and (GetLastError <> ERROR_IO_INCOMPLETE) then
      SerialCheck(sErr);
  ExceptCheck;
end;
{$ENDIF}
// perform synchronous write operation
function TCustomComPort.Write(const Buffer; Count: Integer): Integer;
{$IFDEF MSWINDOWS}
var
  AsyncPtr: PAsync;
begin
  Result := 0;
  InitAsync(AsyncPtr);
  try
    WriteAsync(Buffer, Count, AsyncPtr);
    Result := WaitForAsync(AsyncPtr);
  finally
    DoneAsync(AsyncPtr);
  end;
end;
{$ELSE}
begin
  Result := FileWrite(Fhandle, Buffer, Count);
  serialcheck(result);
  ExceptCheck;
end;
{$ENDIF}

// perform synchronous write operation
function TCustomComPort.WriteStr(const Str: AnsiString): Integer;
{$IFDEF MSWINDOWS}
var
  AsyncPtr: PAsync;
begin
  InitAsync(AsyncPtr);
  try
    WriteStrAsync(Str, AsyncPtr);
    Result := WaitForAsync(AsyncPtr);
  finally
    DoneAsync(AsyncPtr);
  end;
end;
{$ELSE}
begin
  Result := Write(Str[1], Length(Str));
end;
{$ENDIF}

// perform synchronous read operation
function TCustomComPort.Read(var Buffer; Count: Integer): Integer;
{$IFDEF MSWINDOWS}
var
  AsyncPtr: PAsync;
begin
  InitAsync(AsyncPtr);
  try
    ReadAsync(Buffer, Count, AsyncPtr);
    Result := WaitForAsync(AsyncPtr);
  finally
    DoneAsync(AsyncPtr);
  end;
end;
{$ELSE}
begin
  result := FileRead(FHandle, Buffer, Count);
  serialcheck(result);
  ExceptCheck;
end;
{$ENDIF}

// perform synchronous read operation
function TCustomComPort.ReadStr(var Str: AnsiString; Count: Integer): Integer;
{$IFDEF MSWINDOWS}
var
  AsyncPtr: PAsync;
begin
  InitAsync(AsyncPtr);
  try
    ReadStrAsync(Str, Count, AsyncPtr);
    Result := WaitForAsync(AsyncPtr);
    SetLength(Str, Result);
  finally
    DoneAsync(AsyncPtr);
  end;
end;
{$ELSE}
begin
  Result := Read(Str[1], Count);
  SetLength(Str, Result);
end;
{$ENDIF}

// some conversion routines
function BoolToStr(const Value: Boolean): string;
begin
  if Value then
    Result := 'Yes'
  else
    Result := 'No';
end;

function StrToBool(const Value: string): Boolean;
begin
  if UpperCase(Value) = 'YES' then
    Result := True
  else
    Result := False;
end;

function DTRToStr(DTRFlowControl: TDTRFlowControl): string;
const
  DTRStrings: array[TDTRFlowControl] of string = ('Disable', 'Enable',
    'Handshake');
begin
  Result := DTRStrings[DTRFlowControl];
end;

function RTSToStr(RTSFlowControl: TRTSFlowControl): string;
const
  RTSStrings: array[TRTSFlowControl] of string = ('Disable', 'Enable',
    'Handshake', 'Toggle');
begin
  Result := RTSStrings[RTSFlowControl];
end;

function StrToRTS(Str: string): TRTSFlowControl;
var
  I: TRTSFlowControl;
begin
  I := Low(TRTSFlowControl);
  while (I <= High(TRTSFlowControl)) do
  begin
    if UpperCase(Str) = UpperCase(RTSToStr(I)) then
      Break;
    I := Succ(I);
  end;
  if I > High(TRTSFlowControl) then
    Result := rtsDisable
  else
    Result := I;
end;

function StrToDTR(Str: string): TDTRFlowControl;
var
  I: TDTRFlowControl;
begin
  I := Low(TDTRFlowControl);
  while (I <= High(TDTRFlowControl)) do
  begin
    if UpperCase(Str) = UpperCase(DTRToStr(I)) then
      Break;
    I := Succ(I);
  end;
  if I > High(TDTRFlowControl) then
    Result := dtrDisable
  else
    Result := I;
end;

function StrToChar(Str: string): AnsiChar;
var
  A: Integer;
begin
  if Length(Str) > 0 then
  begin
    if (Str[1] = '#') and (Length(Str) > 1) then
    begin
      try
        A := StrToInt(Copy(Str, 2, Length(Str) - 1));
      except
        A := 0;
      end;
      Result := AnsiChar(Chr(Byte(A)));
    end
    else
      Result := AnsiChar(Str[1]);
  end
  else
    Result := #0;
end;

function CharToStr(Ch: AnsiChar): string;
begin
  if Ch in [#33..#127] then
    Result := string(Ch)
  else
    Result := '#' + IntToStr(Ord(Ch));
end;

// store settings to ini file
procedure TCustomComPort.StoreIniFile(IniFile: TIniFile);
begin
  if spBasic in FStoredProps then
  begin
    IniFile.WriteString('SerialPort', 'Port', Port);
    IniFile.WriteString('SerialPort', 'BaudRate', BaudRateToStr(BaudRate));
    if BaudRate = brCustom then
      IniFile.WriteInteger('SerialPort', 'CustomBaudRate', CustomBaudRate);
    IniFile.WriteString('SerialPort', 'StopBits', StopBitsToStr(StopBits));
    IniFile.WriteString('SerialPort', 'DataBits', DataBitsToStr(DataBits));
    IniFile.WriteString('SerialPort', 'Parity', ParityToStr(Parity.Bits));
    IniFile.WriteString('SerialPort', 'FlowControl', FlowControlToStr(FlowControl.FlowControl));
  end;
  if spOthers in FStoredProps then
  begin
    IniFile.WriteString('SerialPort', 'DiscardNull', BoolToStr(DiscardNull));
  end;
  if spParity in FStoredProps then
  begin
    IniFile.WriteString('SerialPort', 'Parity.Check', BoolToStr(Parity.Check));
    IniFile.WriteString('SerialPort', 'Parity.Replace', BoolToStr(Parity.Replace));
    IniFile.WriteString('SerialPort', 'Parity.ReplaceChar', CharToStr(Parity.ReplaceChar));
  end;
  if spBuffer in FStoredProps then
  begin
    IniFile.WriteInteger('SerialPort', 'Buffer.OutputSize', Buffer.OutputSize);
    IniFile.WriteInteger('SerialPort', 'Buffer.InputSize', Buffer.InputSize);
  end;
  {$IFDEF MSWINDOWS}
  if spTimeouts in FStoredProps then
  begin
    IniFile.WriteInteger('SerialPort', 'Timeouts.ReadInterval', Timeouts.ReadInterval);
    IniFile.WriteInteger('SerialPort', 'Timeouts.ReadTotalConstant', Timeouts.ReadTotalConstant);
    IniFile.WriteInteger('SerialPort', 'Timeouts.ReadTotalMultiplier', Timeouts.ReadTotalMultiplier);
    IniFile.WriteInteger('SerialPort', 'Timeouts.WriteTotalConstant', Timeouts.WriteTotalConstant);
    IniFile.WriteInteger('SerialPort', 'Timeouts.WriteTotalMultiplier', Timeouts.WriteTotalMultiplier);
  end;
  {$ENDIF}

  if spFlowControl in FStoredProps then
  begin
    IniFile.WriteString('SerialPort', 'FlowControl.ControlRTS', RTSToStr(FlowControl.ControlRTS));
    IniFile.WriteString('SerialPort', 'FlowControl.ControlDTR', DTRToStr(FlowControl.ControlDTR));
    IniFile.WriteString('SerialPort', 'FlowControl.DSRSensitivity', BoolToStr(FlowControl.DSRSensitivity));
    IniFile.WriteString('SerialPort', 'FlowControl.OutCTSFlow', BoolToStr(FlowControl.OutCTSFlow));
    IniFile.WriteString('SerialPort', 'FlowControl.OutDSRFlow', BoolToStr(FlowControl.OutDSRFlow));
    IniFile.WriteString('SerialPort', 'FlowControl.TxContinueOnXoff', BoolToStr(FlowControl.TxContinueOnXoff));
    IniFile.WriteString('SerialPort', 'FlowControl.XonXoffIn', BoolToStr(FlowControl.XonXoffIn));
    IniFile.WriteString('SerialPort', 'FlowControl.XonXoffOut', BoolToStr(FlowControl.XonXoffOut));
    IniFile.WriteString('SerialPort', 'FlowControl.XoffChar', CharToStr(FlowControl.XoffChar));
    IniFile.WriteString('SerialPort', 'FlowControl.XonChar', CharToStr(FlowControl.XonChar));
  end;
end;

// load settings from ini file
procedure TCustomComPort.LoadIniFile(IniFile: TIniFile);
begin
  if spBasic in FStoredProps then
  begin
    Port := IniFile.ReadString('SerialPort', 'Port', Port);
    BaudRate := StrToBaudRate(IniFile.ReadString('SerialPort', 'BaudRate', BaudRateToStr(BaudRate)));
    if BaudRate = brCustom then
      CustomBaudRate := IniFile.ReadInteger('SerialPort', 'CustomBaudRate', 9600);
    StopBits := StrToStopBits(IniFile.ReadString('SerialPort', 'StopBits', StopBitsToStr(StopBits)));
    DataBits := StrToDataBits(IniFile.ReadString('SerialPort', 'DataBits', DataBitsToStr(DataBits)));
    Parity.Bits := StrToParity(IniFile.ReadString('SerialPort', 'Parity', ParityToStr(Parity.Bits)));
    FlowControl.FlowControl := StrToFlowControl(
      IniFile.ReadString('SerialPort', 'FlowControl', FlowControlToStr(FlowControl.FlowControl)));
  end;
  if spOthers in FStoredProps then
  begin
    DiscardNull := StrToBool(IniFile.ReadString('SerialPort', 'DiscardNull', BoolToStr(DiscardNull)));
  end;
  if spParity in FStoredProps then
  begin
    Parity.Check := StrToBool(IniFile.ReadString('SerialPort', 'Parity.Check', BoolToStr(Parity.Check)));
    Parity.Replace := StrToBool(IniFile.ReadString('SerialPort', 'Parity.Replace', BoolToStr(Parity.Replace)));
    Parity.ReplaceChar := StrToChar(IniFile.ReadString('SerialPort', 'Parity.ReplaceChar', CharToStr(Parity.ReplaceChar)));
  end;
  if spBuffer in FStoredProps then
  begin
    Buffer.OutputSize := IniFile.ReadInteger('SerialPort', 'Buffer.OutputSize', Buffer.OutputSize);
    Buffer.InputSize := IniFile.ReadInteger('SerialPort', 'Buffer.InputSize', Buffer.InputSize);
  end;
  {$IFDEF MSWINDOWS}
  if spTimeouts in FStoredProps then
  begin
    Timeouts.ReadInterval := IniFile.ReadInteger('SerialPort', 'Timeouts.ReadInterval', Timeouts.ReadInterval);
    Timeouts.ReadTotalConstant := IniFile.ReadInteger('SerialPort', 'Timeouts.ReadTotalConstant', Timeouts.ReadTotalConstant);
    Timeouts.ReadTotalMultiplier := IniFile.ReadInteger('SerialPort', 'Timeouts.ReadTotalMultiplier', Timeouts.ReadTotalMultiplier);
    Timeouts.WriteTotalConstant := IniFile.ReadInteger('SerialPort', 'Timeouts.WriteTotalConstant', Timeouts.WriteTotalConstant);
    Timeouts.WriteTotalMultiplier := IniFile.ReadInteger('SerialPort', 'Timeouts.WriteTotalMultiplier', Timeouts.WriteTotalMultiplier);
  end;
  {$ENDIF}
  if spFlowControl in FStoredProps then
  begin
    FlowControl.ControlRTS := StrToRTS(IniFile.ReadString('SerialPort', 'FlowControl.ControlRTS', RTSToStr(FlowControl.ControlRTS)));
    FlowControl.ControlDTR := StrToDTR(IniFile.ReadString('SerialPort', 'FlowControl.ControlDTR', DTRToStr(FlowControl.ControlDTR)));
    FlowControl.DSRSensitivity := StrToBool(IniFile.ReadString('SerialPort', 'FlowControl.DSRSensitivity', BoolToStr(FlowControl.DSRSensitivity)));
    FlowControl.OutCTSFlow := StrToBool(IniFile.ReadString('SerialPort', 'FlowControl.OutCTSFlow', BoolToStr(FlowControl.OutCTSFlow)));
    FlowControl.OutDSRFlow := StrToBool(IniFile.ReadString('SerialPort', 'FlowControl.OutDSRFlow', BoolToStr(FlowControl.OutCTSFlow)));
    FlowControl.TxContinueOnXoff := StrToBool(IniFile.ReadString('SerialPort', 'FlowControl.TxContinueOnXoff', BoolToStr(FlowControl.TxContinueOnXoff)));
    FlowControl.XonXoffIn := StrToBool(IniFile.ReadString('SerialPort', 'FlowControl.XonXoffIn', BoolToStr(FlowControl.XonXoffIn)));
    FlowControl.XonXoffOut := StrToBool(IniFile.ReadString('SerialPort', 'FlowControl.XonXoffOut', BoolToStr(FlowControl.XonXoffOut)));
    FlowControl.XoffChar := StrToChar(IniFile.ReadString('SerialPort', 'FlowControl.XoffChar', CharToStr(FlowControl.XoffChar)));
    FlowControl.XonChar := StrToChar(IniFile.ReadString('SerialPort', 'FlowControl.XonChar', CharToStr(FlowControl.XonChar)));
  end;
end;

// store settings
procedure TCustomComPort.StoreSettings(StoreTo: string);
var
  IniFile: TIniFile;
begin
  try
      IniFile := TIniFile.Create(StoreTo);
      try
        StoreIniFile(IniFile);
      finally
        IniFile.Free;
      end

  except
    RaiseSynaError(ErrStoreSettings);
  end;
end;

// load settings
procedure TCustomComPort.LoadSettings(LoadFrom: string);
var
  IniFile: TIniFile;
begin
  BeginUpdate;
  try
    try
        IniFile := TIniFile.Create(LoadFrom);
        try
          LoadIniFile(IniFile);
        finally
          IniFile.Free;
        end

    finally
      EndUpdate;
    end;
  except
    RaiseSynaError(ErrLoadSettings);
  end;
end;

procedure TCustomComPort.BaudRateBoxChange(Sender: TObject);
var Form : TForm;
    Edt : TEdit;
begin
  Form := ( (Sender as TComboBox).Parent as TForm);
  if Assigned (Form) then
  begin
    Edt := (Form.FindChildControl('Edit0') as TEdit);
    if Assigned(Edt) then
    Edt.Enabled := ((Sender as TComboBox).ItemIndex = 0);
  end;
end;

procedure TCustomComPort.BaudRateEditKeyPress(Sender: TObject; var Key: char);
begin
  if not (Key in ['0'..'9',#8]) then Key:=#0;
end;

procedure TCustomComPort.BaudRateEditExit(Sender: TObject);
begin
  if (StrToIntDef( (Sender as TEdit).Text, 0 ) = 0) then
  (Sender as TEdit).Text := IntToStr( (Sender as TEdit).Tag );
end;

function TCustomComPort.SetupPortDialog(const SettingsFileName: String): boolean;
var Form: TForm;
    Lbl: array[0..5] of TLabel;
    Box: array[0..5] of TComboBox;
    Edt: TEdit;
    Btn: array[0..1] of TButton;
    i: integer;
begin
  result := false;
  // форма:
  Form := TForm.Create(Application);
  try
    Form.Name:='Form_Port_Setup';
    Form.BorderStyle := bsDialog;
    Form.Caption:='Настройки порта';
    Form.ClientHeight := 280;
    Form.ClientWidth := 320;
    Form.Position := poScreenCenter;

    // компоненты формы:
    for i:=0 to 5 do
    begin
      Lbl[i] := TLabel.Create( Form );
      Lbl[i].Parent := Form;
      Lbl[i].Name := 'Label'+IntToStr(i);
      Lbl[i].Left := 16;
      Lbl[i].Top := 15 + i*34;
      Lbl[i].AutoSize := true;
      //
      Box[i] := TComboBox.Create(Form);
      Box[i].Parent := Form;
      Box[i].Name := 'ComboBox'+IntToStr(i);
      Box[i].Left := 125;
      Box[i].Top := 10 + i*34;
      if i = 1 then
      begin
        Box[i].Width := 90;
        Box[i].OnChange := BaudRateBoxChange;
        Edt := TEdit.Create(Form);
        Edt.Parent := Form;
        Edt.Name := 'Edit0';
        Edt.Left := Box[i].Left + Box[i].Width + 8;
        Edt.Top := Box[i].Top;
        Edt.Width := Form.ClientWidth - Edt.Left - 16;
        Edt.Text := IntToStr(FCustomBaudRate);
        Edt.OnKeyPress := BaudRateEditKeyPress;
        Edt.OnExit := BaudRateEditExit;
      end else
      begin
        Box[i].Width := Form.ClientWidth - Box[i].Left - 16;
      end;
      if i = 0 then
      Box[i].Style := csDropDown
      else
      Box[i].Style := csDropDownList;
    end;

    // кнопки:
    for i:=0 to 1 do
    begin
      Btn[i] := TButton.Create(Form);
      Btn[i].Parent := Form;
      Btn[i].Name := 'Button'+IntToStr(i);
      Btn[i].Top := Form.ClientHeight - Btn[i].Height - 16;
      Btn[i].Width := 90;
    end;
    i := Form.ClientWidth div 2;
    Btn[0].Left := i - Btn[0].Width - 5;
    Btn[1].Left := i + 5;
    Btn[0].Caption := 'OK';
    Btn[1].Caption := 'Отмена';
    Btn[0].ModalResult := mrOK;
    Btn[1].ModalResult := mrCancel;

    // заполняем настройки:
    // Список доступных портов:
    Lbl[0].Caption:='Порт:';
    i := GetPortsList( Box[0].Items );
    if i>=0 then Box[0].ItemIndex := i;
    // Список Скоростей:
    Lbl[1].Caption:='Скорость:';
    i := GetBaudRatesList( Box[1].Items );
    if i>=0 then Box[1].ItemIndex := i;
    Edt.Tag := FCustomBaudRate;
    Edt.Enabled := (i=0);
    // Список Бит данных:
    Lbl[2].Caption:='Биты данных:';
    i := GetDataBitsList( Box[2].Items );
    if i>=0 then Box[2].ItemIndex := i;
    // Список Стоповых бит:
    Lbl[3].Caption:='Стоповые биты:';
    i := GetStopBitsList( Box[3].Items );
    if i>=0 then Box[3].ItemIndex := i;
    // Список Четности:
    Lbl[4].Caption:='Четность:';
    i := GetParityList( Box[4].Items );
    if i>=0 then Box[4].ItemIndex := i;
    // Список контроль:
    Lbl[5].Caption:='Контроль:';
    i := GetFlowControlList( Box[5].Items );
    if i>=0 then Box[5].ItemIndex := i;

    // выводим форму настроек и ждем результата:
    if (Form.ShowModal = mrOK) then
    begin
      {если нажали ОК, применяем настройки из Комбобоксов к порту:}
      BeginUpdate;

      FPort := Box[0].Text;
      FBaudRate := StrToBaudRate(Box[1].Text);
      if Box[1].ItemIndex=0 then
      begin
        FCustomBaudRate := StrToIntDef(Edt.Text, Edt.Tag);
      end else
      begin
        FCustomBaudRate := StrToIntDef(Box[1].Text, Edt.Tag);
      end;
      FDataBits := StrToDataBits(Box[2].Text);
      FStopBits := StrToStopBits(Box[3].Text);
      FParity.Bits := StrToParity(Box[4].Text);
      FFlowControl.FlowControl := StrToFlowControl(Box[5].Text);

      EndUpdate;

      // Сохраняем настройки в файл:
      StoreSettings( SettingsFileName );
      result := true;
    end;

  finally
    Form.Free;
  end;
end;

function TCustomComPort.GetSettingsStr: string;
begin
  result := FPort+':';
  if (FBaudRate = brCustom) then
  result := result + IntToStr(FCustomBaudRate)
  else
  result := result + BaudRateToStr(FBaudRate);
  result := result+','+
            DataBitsToStr(FDataBits)+','+
            StopBitsToStr(FStopBits);
  if Assigned(FParity) then
  Result := Result + ',' + ParityToStr(FParity.Bits);
end;

function TCustomComPort.GetPortsList(List: TStrings): integer;
var tmp: TStringList;
    i, n: integer;
begin
  tmp:=TStringList.Create;
  result := -1;
  try
    EnumComPorts(tmp);
    List.Assign(tmp);

    n := tmp.Count-1;
    for i:=0 to n do
    begin
      if (tmp.Strings[i] = FPort) then
      begin
        result:=i;
        break;
      end;
    end;
  finally
    tmp.Free;
  end;
end;

function TCustomComPort.GetBaudRatesList(List: TStrings): integer;
var tmp: TStringList;
    I: TBaudRate;
    n: integer;
begin
  tmp:=TStringList.Create;
  result := -1;
  try
    I := Low(TBaudRate);
    while (I <= High(TBaudRate)) do
    begin
      n := tmp.Add( BaudRateToStr(I) );
      if (FBaudRate = I) then result := n;
      I := Succ(I);
    end;
    List.Assign(tmp);
  finally
    tmp.Free;
  end;
end;

function TCustomComPort.GetDataBitsList(List: TStrings): integer;
var tmp: TStringList;
    I: TDataBits;
    n: integer;
begin
  tmp:=TStringList.Create;
  result := -1;
  try
    I := Low(TDataBits);
    while (I <= High(TDataBits)) do
    begin
      n := tmp.Add( DataBitsToStr(I) );
      if (FDataBits = I) then result := n;
      I := Succ(I);
    end;
    List.Assign(tmp);
  finally
    tmp.Free;
  end;
end;

function TCustomComPort.GetStopBitsList(List: TStrings): integer;
var tmp: TStringList;
    I: TStopBits;
    n: integer;
begin
  tmp:=TStringList.Create;
  result := -1;
  try
    I := Low(TStopBits);
    while (I <= High(TStopBits)) do
    begin
      n := tmp.Add( StopBitsToStr(I) );
      if (FStopBits = I) then result := n;
      I := Succ(I);
    end;
    List.Assign(tmp);
  finally
    tmp.Free;
  end;
end;

function TCustomComPort.GetParityList(List: TStrings): integer;
var tmp: TStringList;
    I: TParityBits;
    n: integer;
begin
  tmp:=TStringList.Create;
  result := -1;
  try
    I := Low(TParityBits);
    while (I <= High(TParityBits)) do
    begin
      n := tmp.Add( ParityToStr(I) );
      if (FParity.Bits = I) then result := n;
      I := Succ(I);
    end;
    List.Assign(tmp);
  finally
    tmp.Free;
  end;
end;

function TCustomComPort.GetFlowControlList(List: TStrings): integer;
var tmp: TStringList;
    I: TFlowControl;
    n: integer;
begin
  tmp:=TStringList.Create;
  result := -1;
  try
    I := Low(TFlowControl);
    while (I <= High(TFlowControl)) do
    begin
      n := tmp.Add( FlowControlToStr(I) );
      if (FFlowControl.FlowControl = I) then result := n;
      I := Succ(I);
    end;
    List.Assign(tmp);
  finally
    tmp.Free;
  end;
end;

// default actions on port events
procedure TCustomComPort.DoBeforeClose;
begin
  if Assigned(FOnBeforeClose) then
    FOnBeforeClose(Self);
end;

procedure TCustomComPort.DoBeforeOpen;
begin
  if Assigned(FOnBeforeOpen) then
    FOnBeforeOpen(Self);
end;

procedure TCustomComPort.DoAfterOpen;
begin
  if Assigned(FOnAfterOpen) then
    FOnAfterOpen(Self);
end;

procedure TCustomComPort.DoAfterClose;
begin
  if Assigned(FOnAfterClose) then
    FOnAfterClose(Self);
end;

procedure TCustomComPort.DoRxChar;
begin
  if Assigned(FOnRxChar) then
    FOnRxChar(Self);
end;

procedure TCustomComPort.DoRing;
begin
  if Assigned(FOnRing) then
    FOnRing(Self);
end;

procedure TCustomComPort.DoCTSChange(OnOff: Boolean);
begin
  if Assigned(FOnCTSChange) then
    FOnCTSChange(Self, OnOff);
end;

procedure TCustomComPort.DoDSRChange(OnOff: Boolean);
begin
  if Assigned(FOnDSRChange) then
    FOnDSRChange(Self, OnOff);
end;

procedure TCustomComPort.DoRLSDChange(OnOff: Boolean);
begin
  if Assigned(FOnRLSDChange) then
    FOnRLSDChange(Self, OnOff);
end;

procedure TCustomComPort.DoError(ErrorCode: integer; ErrorMessage: string);
begin
  if Assigned(FOnError) then
    FOnError(Self, ErrorCode, ErrorMessage);
end;

// set signals to false on close, and to proper value on open,
// because OnXChange events are not called automatically
procedure TCustomComPort.CheckSignals(Open: Boolean);
begin
  if Open then
  begin
    CallCTSChange;
    CallDSRChange;
    CallRLSDChange;
  end else
  begin
    DoCTSChange(False);
    DoDSRChange(False);
    DoRLSDChange(False);
  end;
end;

// called in response to EV_X events, except CallXClose, CallXOpen
procedure TCustomComPort.CallAfterClose;
begin
  DoAfterClose;
end;

procedure TCustomComPort.CallAfterOpen;
begin
  DoAfterOpen;
  CheckSignals(True);
end;

procedure TCustomComPort.CallBeforeClose;
begin
  // shutdown com signals manually
  CheckSignals(False);
  DoBeforeClose;
end;

procedure TCustomComPort.CallBeforeOpen;
begin
  DoBeforeOpen;
end;

procedure TCustomComPort.CallCTSChange;
var
  OnOff: Boolean;
begin
  OnOff := csCTS in Signals;
  DoCTSChange(OnOff);
end;

procedure TCustomComPort.CallDSRChange;
var
  OnOff: Boolean;
begin
  OnOff := csDSR in Signals;
  DoDSRChange(OnOff);
end;

procedure TCustomComPort.CallRLSDChange;
var
  OnOff: Boolean;
begin
  OnOff := csRLSD in Signals;
  DoRLSDChange(OnOff);
end;

procedure TCustomComPort.CallRing;
begin
  DoRing;
end;

procedure TCustomComPort.CallRxChar;
begin
{$IFDEF MSWINDOWS}
  if InputCount > 0 then
{$ENDIF}
  DoRxChar;
end;

procedure TCustomComPort.CallError;
{$IFDEF MSWINDOWS}
var
  Errors: DWORD;
  ComStat: TComStat;
  s: string;
{$ENDIF}
begin
  if (FLastError <> sOK) then
  begin
    DoError(FLastError, FLastErrorDesc);
{$IFDEF MSWINDOWS}
  end else
  if ClearCommError(FHandle, Errors, @ComStat) and (evError in FEvents) then
  begin
    s := '';
    if (CE_FRAME and Errors) <> 0 then s:=s+'Frame error. ';
    if ((CE_RXPARITY and Errors) <> 0) and FParity.Check then // get around a bug
    s := s + 'Parity Error. ';
    if (CE_OVERRUN and Errors) <> 0 then s:=s+'Overrun error. ';
    if (CE_RXOVER and Errors) <> 0 then s:=s+'Rxover error. ';
    if (CE_TXFULL and Errors) <> 0 then s:=s+'Txfull error. ';
    if (CE_BREAK and Errors) <> 0 then s:=s+'Break error. ';
    if (CE_IOE and Errors) <> 0 then s:=s+'Ioe error. ';
    if (CE_MODE and Errors) <> 0 then s:=s+'Mode error. ';

    if (s <> '') then
    DoError(-1, s);
{$ENDIF}
  end;
end;

// set connected property, same as Open/Close methods
procedure TCustomComPort.SetConnected(const Value: Boolean);
begin
  if not ((csDesigning in ComponentState) or (csLoading in ComponentState)) then
  begin
    if Value <> FConnected then
      if Value then
        Open
      else
        Close;
  end;
end;

// set baud rate
procedure TCustomComPort.SetBaudRate(const Value: TBaudRate);
begin
  if Value <> FBaudRate then
  begin
    FBaudRate := Value;
    // if possible, apply settings
    ApplyDCB;
  end;
end;

// set custom baud rate
procedure TCustomComPort.SetCustomBaudRate(const Value: Integer);
begin
  if Value <> FCustomBaudRate then
  begin
    FCustomBaudRate := Value;
    ApplyDCB;
  end;
end;

// set data bits
procedure TCustomComPort.SetDataBits(const Value: TDataBits);
begin
  if Value <> FDataBits then
  begin
    FDataBits := Value;
    ApplyDCB;
  end;
end;

// set discard null charachters
procedure TCustomComPort.SetDiscardNull(const Value: Boolean);
begin
  if Value <> FDiscardNull then
  begin
    FDiscardNull := Value;
    ApplyDCB;
  end;
end;

// set port
procedure TCustomComPort.SetNewPort(const Value: TPort);
begin
  if Value <> FPort then
  begin
    FPort := Value;
    if FConnected and not ((csDesigning in ComponentState) or
      (csLoading in ComponentState)) then
    begin
      Close;
      Open;
    end;
  end;
end;

// set stop bits
procedure TCustomComPort.SetStopBits(const Value: TStopBits);
begin
  if Value <> FStopBits then
  begin
    FStopBits := Value;
    ApplyDCB;
  end;
end;

// set flow control
procedure TCustomComPort.SetFlowControl(const Value: TComFlowControl);
begin
  FFlowControl.Assign(Value);
  ApplyDCB;
end;

// set parity
procedure TCustomComPort.SetParity(const Value: TComParity);
begin
  FParity.Assign(Value);
  ApplyDCB;
end;

{$IFDEF MSWINDOWS}
// set timeouts
procedure TCustomComPort.SetTimeouts(const Value: TComTimeouts);
begin
  FTimeouts.Assign(Value);
  ApplyTimeouts;
end;
{$ENDIF}

function TCustomComPort.SerialCheck(SerialResult: integer): integer;
begin
  if SerialResult = integer(INVALID_HANDLE_VALUE) then
{$IFDEF MSWINDOWS}
    result := GetLastError
{$ELSE}
  {$IFNDEF FPC}
    result := GetLastError
  {$ELSE}
    result := fpGetErrno
  {$ENDIF}
{$ENDIF}
  else
    result := sOK;
  FLastError := result;
  FLastErrorDesc := GetErrorDesc(FLastError);
end;

procedure TCustomComPort.ExceptCheck;
var
  e: ESynaSerError;
begin
  if (FLastError <> sOK) then
  begin
    if FRaiseExcept then
    begin
      e := ESynaSerError.CreateFmt('Communication error %d: %s', [FLastError, FLastErrorDesc]);
      e.ErrorCode := FLastError;
      e.ErrorMessage := FLastErrorDesc;
      raise e;
    end;

    if (evError in FEvents) then CallError;

    SetSynaError(sOK);
  end;
end;

function TCustomComPort.GetErrorDesc(ErrorCode: integer): string;
begin
  Result:= '';
  case ErrorCode of
    sOK:               Result := 'OK';
    ErrAlreadyOwned:   Result := 'Port owned by other process';
    ErrAlreadyInUse:   Result := 'Instance already in use';
    ErrWrongParameter: Result := 'Wrong parameter at call';
    ErrPortNotOpen:    Result := 'Instance not yet connected';
    ErrNoDeviceAnswer: Result := 'No device answer detected';
    ErrMaxBuffer:      Result := 'Maximal buffer length exceeded';
    ErrTimeout:        Result := 'Timeout during operation';
    ErrNotRead:        Result := 'Reading of data failed';
    ErrFrame:          Result := 'Receive framing error';
    ErrOverrun:        Result := 'Receive Overrun Error';
    ErrRxOver:         Result := 'Receive Queue overflow';
    ErrRxParity:       Result := 'Receive Parity Error';
    ErrTxFull:         Result := 'Tranceive Queue is full';
    ErrAsyncNil:       Result := 'Invalid Async parameter';
    ErrStoreSettings:  Result := 'Store settings failed';
    ErrLoadSettings:   Result := 'Load settings failed';

  end;
  if Result = '' then
  begin
    Result := SysErrorMessage(ErrorCode);
  end;
end;

procedure TCustomComPort.SetSynaError(ErrNumber: integer);
begin
  FLastError := ErrNumber;
  FLastErrorDesc := GetErrorDesc(FLastError);
end;

procedure TCustomComPort.RaiseSynaError(ErrNumber: integer);
begin
  SetSynaError(ErrNumber);
  ExceptCheck;
end;

// set buffer
procedure TCustomComPort.SetBuffer(const Value: TComBuffer);
begin
  FBuffer.Assign(Value);
  ApplyBuffer;
end;

(*****************************************
 * other procedures/functions            *
 *****************************************)

{$IFDEF MSWINDOWS}
// initialization of PAsync variables used in asynchronous calls
procedure InitAsync(var AsyncPtr: PAsync);
begin
  New(AsyncPtr);
  with AsyncPtr^ do
  begin
    FillChar(Overlapped, SizeOf(TOverlapped), 0);
    Overlapped.hEvent := CreateEvent(nil, True, True, nil);
    Data := nil;
    Size := 0;
  end;
end;

// clean-up of PAsync variable
procedure DoneAsync(var AsyncPtr: PAsync);
begin
  with AsyncPtr^ do
  begin
    CloseHandle(Overlapped.hEvent);
    if Data <> nil then
      FreeMem(Data);
  end;
  Dispose(AsyncPtr);
  AsyncPtr := nil;
end;

procedure EnumComPorts(Ports: TStrings);
var
  reg: TRegistry;
  l, tmp: TStringList;
  n: integer;
begin
  tmp := TStringList.Create;
  l := TStringList.Create;
  reg := TRegistry.Create;
  try
    reg.Access := KEY_READ;
    reg.RootKey := HKEY_LOCAL_MACHINE;
    reg.OpenKey('\HARDWARE\DEVICEMAP\SERIALCOMM', false);
    reg.GetValueNames(l);
    for n := 0 to l.Count - 1 do
    begin
      tmp.Add(reg.ReadString(l[n]));
    end;
    tmp.Sort;
    Ports.Assign(tmp);
  finally
    reg.Free;
    l.Free;
    tmp.Free;
  end;
end;
{$ELSE}
procedure EnumComPorts(Ports: TStrings);
var
  tmp: TStringList;
  AProcess: TProcess;
  AStringList: TStringList;
  i, n, k: integer;
  str:string;
begin
  tmp:=TStringList.Create;
  for k:=0 to 9 do  begin
          str:='/dev/ttyUSB'+IntToStr(k);
          if FileExists(str) then tmp.Add(str);
          str:='/dev/ttyACM'+IntToStr(k);
          if FileExists(str) then tmp.Add(str);
        end;
  try
    AProcess := TProcess.Create(nil);
    AStringList:= TStringList.Create;
    try
      AProcess.Executable := '/bin/sh';
      AProcess.Parameters.Add('-c');
      AProcess.Parameters.Add('dmesg | grep ttyS');
      AProcess.Options := AProcess.Options + [poWaitOnExit, poUsePipes, poNoConsole];
      AProcess.Execute;
      AStringList.LoadFromStream(AProcess.Output);
      //
      n := AStringList.Count-1;
      for i:=0 to n do
      begin
        k := Pos('ttyS', AStringList.Strings[i]);
        if k>0 then
        begin
          str:='/dev/ttyS';
          k := k + 4;
          while (  (k<=Length(AStringList.Strings[i])) and
                   (StrToIntDef( Copy(AStringList.Strings[i], k, 1), -1 ) >= 0) ) do
          begin
            str := str + Copy(AStringList.Strings[i], k, 1);
            Inc(k);
          end;
          if (Length(str) > 9) and FileExists(str) then tmp.Add(str);
        end;
      end;

    finally
      AStringList.Free;
      AProcess.Free;
    end;
    //tmp.Sort;
    Ports.Assign(tmp);
  finally
    tmp.Free;
  end;
end;
{$ENDIF}

// string to baud rate
function StrToBaudRate(Str: string): TBaudRate;
var
  I: TBaudRate;
begin
  I := Low(TBaudRate);
  while (I <= High(TBaudRate)) do
  begin
    if UpperCase(Str) = UpperCase(BaudRateToStr(TBaudRate(I))) then
      Break;
    I := Succ(I);
  end;
  if I > High(TBaudRate) then
    Result := brCustom
  else
    Result := I;
end;

// string to stop bits
function StrToStopBits(Str: string): TStopBits;
var
  I: TStopBits;
begin
  I := Low(TStopBits);
  while (I <= High(TStopBits)) do
  begin
    if UpperCase(Str) = UpperCase(StopBitsToStr(TStopBits(I))) then
      Break;
    I := Succ(I);
  end;
  if I > High(TStopBits) then
    Result := sbOneStopBit
  else
    Result := I;
end;

// string to data bits
function StrToDataBits(Str: string): TDataBits;
var
  I: TDataBits;
begin
  I := Low(TDataBits);
  while (I <= High(TDataBits)) do
  begin
    if UpperCase(Str) = UpperCase(DataBitsToStr(I)) then
      Break;
    I := Succ(I);
  end;
  if I > High(TDataBits) then
    Result := dbEight
  else
    Result := I;
end;

// string to parity
function StrToParity(Str: string): TParityBits;
var
  I: TParityBits;
begin
  I := Low(TParityBits);
  while (I <= High(TParityBits)) do
  begin
    if UpperCase(Str) = UpperCase(ParityToStr(I)) then
      Break;
    I := Succ(I);
  end;
  if I > High(TParityBits) then
    Result := prNone
  else
    Result := I;
end;

// string to flow control
function StrToFlowControl(Str: string): TFlowControl;
var
  I: TFlowControl;
begin
  I := Low(TFlowControl);
  while (I <= High(TFlowControl)) do
  begin
    if UpperCase(Str) = UpperCase(FlowControlToStr(I)) then
      Break;
    I := Succ(I);
  end;
  if I > High(TFlowControl) then
    Result := fcCustom
  else
    Result := I;
end;

// baud rate to string
function BaudRateToStr(BaudRate: TBaudRate): string;
const
  BaudRateStrings: array[TBaudRate] of string = ('Custom', '110', '300', '600', '1200',
                             '2400', '4800', '9600', '19200', '38400', '57600', '115200');
begin
  Result := BaudRateStrings[BaudRate];
end;

// stop bits to string
function StopBitsToStr(StopBits: TStopBits): string;
const
  StopBitsStrings: array[TStopBits] of string = ('1', '1.5', '2');
begin
  Result := StopBitsStrings[StopBits];
end;

// data bits to string
function DataBitsToStr(DataBits: TDataBits): string;
const
  DataBitsStrings: array[TDataBits] of string = ('5', '6', '7', '8');
begin
  Result := DataBitsStrings[DataBits];
end;

// parity to string
function ParityToStr(Parity: TParityBits): string;
const
  ParityBitsStrings: array[TParityBits] of string = ('None', 'Odd', 'Even',
    'Mark', 'Space');
begin
  Result := ParityBitsStrings[Parity];
end;

// flow control to string
function FlowControlToStr(FlowControl: TFlowControl): string;
const
  FlowControlStrings: array[TFlowControl] of string = ('Hardware',
    'Software', 'None', 'Custom');
begin
  Result := FlowControlStrings[FlowControl];
end;

end.