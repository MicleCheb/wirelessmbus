program project1;

{$mode objfpc}{$H+}

{$IFDEF LiNUX}
  {$IFNDEF UseCThreads}
    {$DEFINE UseCThreads}
  {$ENDIF}
{$ENDIF}

uses
  {$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, unitMain, unitSplash, unitData, unitAbout, unitlog,
  unitSetup
  { you can add units after this };

{$R *.res}

begin
  RequireDerivedFormResource := True;
  Application.Initialize;
  with TFormSplash.Create(nil) do
  try
    Show;
    Update;
    Application.CreateForm(TFormMain, FormMain);
    Application.CreateForm(TFormLog, FormLog);
    Application.CreateForm(TData, Data);
  finally
    Free;
  end;
  Application.Run;
end.

