unit unitSplash;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls;

type

  { TFormSplash }

  TFormSplash = class(TForm)
    Image1: TImage;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

implementation

{$R *.lfm}

{ TFormSplash }

procedure TFormSplash.FormCreate(Sender: TObject);
begin
  AutoScroll := False;
  Label1.Caption:='Загрузка приложения.'#13#10'Пожалуйста, подождите...';
  Label1.BringToFront;
end;

end.

