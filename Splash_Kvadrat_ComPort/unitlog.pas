unit unitlog;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  IniFiles, LMessages, ExtCtrls;

type

  { TFormLog }

  TFormLog = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Edit1: TEdit;
    LogMemo: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Edit1Exit(Sender: TObject);
    procedure Edit1KeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
  private
    { private declarations }
  protected
    // перехват сообщения изменения размеров формы:
    procedure WMSize(var Message: TLMSize); message LM_SIZE;
    // перехват сообщения изменения позиции формы:
    procedure WMPosChanged(var Message: TLMWindowPosChanged);
      message LM_WINDOWPOSCHANGED;
  public
    { public declarations }
    procedure LoadSettings(const FileName: string);
    procedure SaveSettings(const FileName: string);
    procedure AddToLog(const Str: string);
  end;

var
  FormLog: TFormLog;

implementation

uses
  unitData;

{$R *.lfm}

procedure TFormLog.WMPosChanged(var Message: TLMWindowPosChanged);
begin // изменение позиции окна:
  inherited WMWindowPosChanged(Message);
  if Assigned(Data) then
    SaveSettings(Data.SettingsFileName);
end;

procedure TFormLog.WMSize(var Message: TLMSize);
begin // изменение размеров окна:
  inherited WMSize(Message);
  if Assigned(Data) then
    SaveSettings(Data.SettingsFileName);
  Application.ProcessMessages;
end;

procedure TFormLog.Button1Click(Sender: TObject);
begin
  // кнопка очистки лога
  LogMemo.Lines.Clear;
end;

procedure TFormLog.Button2Click(Sender: TObject);
begin
  // кнопка закрыть
  Close;
end;

procedure TFormLog.Button3Click(Sender: TObject);
var
  buf: array[0..2047] of byte;
  n, Count: integer;
  str: string;
  val: byte;
begin
  // кнопка отправить >>
  str := Trim(Edit1.Text);
  if (str = '') then
    Exit;

  Count := 0;

  while (Length(str) > 0) do
  begin
    n := Pos(' ', str);
    if (n > 0) then
    begin
      val := StrToIntDef('$' + Copy(str, 1, n - 1), $FF);
      Delete(str, 1, n);
    end
    else
    begin
      val := StrToIntDef('$' + str, $FF);
      str := '';
    end;

    if (val >= 0) then
    begin
      buf[Count] := val;
      Inc(Count);
    end;

    str := Trim(str);
  end;

  if (Count > 0) and Assigned(Data) then
    Data.WriteToPort(buf, Count);
end;

procedure TFormLog.Edit1Exit(Sender: TObject);
var
  str: string;
begin
  if (Edit1.Text <> '') then
  begin
    str := Edit1.Text;
    if (str[length(str) - 1] = ' ') then
      str := Copy(str, 1, length(str) - 1) + '0' + str[length(str)];
    Edit1.Text := str;
  end;
end;

procedure TFormLog.Edit1KeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
var
  str: string;
begin
  case Key of
    48..57, 65..70:
    begin
      str := Edit1.Text;
      if (Length(str) = 0) then
        str := Chr(Key)
      else
      begin
        if (AnsiChar(str[Length(str)]) in ['0'..'9', 'A'..'F']) and
          (AnsiChar(str[Length(str) - 1]) in ['0'..'9', 'A'..'F']) then
          str := str + ' ' + Chr(Key)
        else
          str := str + Chr(Key);
      end;
      Edit1.Text := str;
      Edit1.SelStart := Length(Edit1.Text);
    end;
    8:
    begin
      str := Edit1.Text;
      if Length(str) > 0 then
      begin
        Delete(str, Length(str), 1);
        while (Length(str) > 0) and (str[Length(str)] = ' ') do
          Delete(str, Length(str), 1);
        Edit1.Text := str;
      end;
      Edit1.SelStart := Length(Edit1.Text);
    end;
    46:
    begin
      Edit1.Clear;
    end;
  end;
end;

procedure TFormLog.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  if Assigned(Data) then
    Data.ConsoleItem.Checked := False;
end;

procedure TFormLog.LoadSettings(const FileName: string);
begin
  // Загрузка настроек
  with TIniFile.Create(FileName) do
    try
      Width := ReadInteger('FormLog', 'Width', 440);
      Height := ReadInteger('FormLog', 'Height', 300);
      Top := ReadInteger('FormLog', 'Top', 200);
      Left := ReadInteger('FormLog', 'Left', 300);
    finally
      Free;
    end;
end;

procedure TFormLog.SaveSettings(const FileName: string);
begin
  // Сохранение настроек
  with TIniFile.Create(FileName) do
    try
      WriteInteger('FormLog', 'Width', Width);
      WriteInteger('FormLog', 'Height', Height);
      WriteInteger('FormLog', 'Top', Top);
      WriteInteger('FormLog', 'Left', Left);
    finally
      Free;
    end;
end;

procedure TFormLog.AddToLog(const Str: string);
begin
  LogMemo.Lines.BeginUpdate;
  if LogMemo.Lines.Count > 1500 then
    while LogMemo.Lines.Count > 1000 do
      LogMemo.Lines.Delete(0);
  LogMemo.Lines.EndUpdate;
  LogMemo.Lines.Add(Str);
end;

end.
