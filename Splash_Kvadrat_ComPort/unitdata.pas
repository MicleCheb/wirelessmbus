
//old Delphi does not have MSWINDOWS define.
{$IFDEF WIN32}
  {$IFNDEF MSWINDOWS}
    {$DEFINE MSWINDOWS}
  {$ENDIF}
{$ENDIF}

unit unitData;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Menus,
  IniFiles,
  // модуль COM-порта:
  naykSerial;

const
  sAppTitle = 'Шаблон приложения для работы с COM-портом';
  sAuthor = 'Тетерин Евгений';
  sVer = '1.0 beta (2014)';
  sMail = 'nayk@nxt.ru';
  sIcq = '262088737';

  sSettingsDir = 'SerialPortTemplate';

type

  { TData }

  TData = class(TDataModule)
    MenuImages: TImageList;
    MainMenu1: TMainMenu;
    FileMenuItem: TMenuItem;
    ExitItem: TMenuItem;
    EditMenuItem: TMenuItem;
    ActMenuItem: TMenuItem;
    ConsoleItem: TMenuItem;
    HelpMenuItem: TMenuItem;
    AboutItem: TMenuItem;
    PanelItem: TMenuItem;
    SetupItem: TMenuItem;
    ViewMenuItem: TMenuItem;
    PortItem: TMenuItem;
    PortSetupItem: TMenuItem;
    procedure AboutMenuItemClick(Sender: TObject);
    procedure ConsoleItemClick(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure ExitItemClick(Sender: TObject);
    procedure PanelItemClick(Sender: TObject);
    procedure PortItemClick(Sender: TObject);
    procedure PortSetupItemClick(Sender: TObject);
    procedure SetupItemClick(Sender: TObject);
  private
    { private declarations }
    SerialPort: TnaykSerial; // - компонент ком-порта
    // События ком-порта:
    procedure SerialPortRxData(Sender: TObject);
    procedure SerialPortBeforeOpen(Sender: TObject);
    procedure SerialPortAfterOpen(Sender: TObject);
    procedure SerialPortBeforeClose(Sender: TObject);
    procedure SerialPortAfterClose(Sender: TObject);
    procedure SerialPortError(Sender: TObject; ErrorCode: integer; ErrorMessage: string);

  public
    { public declarations }
    SettingsFileName: string;
    function PortIsOpen: boolean;
    function OpenPort: boolean;
    function ClosePort: boolean;
    function SetupPort: boolean;
    function GetPortSettingsStr: string;
    function WriteToPort(buf: array of byte; Count: integer): integer;
  end;

var
  Data: TData;

implementation

uses
  unitMain, unitAbout, unitlog, unitSetup;

{$R *.lfm}

{ TData }

procedure TData.DataModuleCreate(Sender: TObject);
begin
  // имя файла настроек:
  SettingsFileName := GetUserDir;
  {$IFDEF MSWINDOWS}
  SettingsFileName := IncludeTrailingPathDelimiter(IncludeTrailingPathDelimiter(
    SettingsFileName + 'Application Data') + sSettingsDir);
  {$ELSE}
  SettingsFileName := IncludeTrailingPathDelimiter(IncludeTrailingPathDelimiter(
    SettingsFileName + '.config') + sSettingsDir);
  {$ENDIF}
  ForceDirectories(SettingsFileName);
  SettingsFileName := SettingsFileName + 'settings.cfg';

  // создаем экземпляр класса компорта и назначаем ему обработчики событий:
  SerialPort := TnaykSerial.Create(Self);
  SerialPort.OnRxChar := @SerialPortRxData;
  SerialPort.OnBeforeOpen := @SerialPortBeforeOpen;
  SerialPort.OnAfterOpen := @SerialPortAfterOpen;
  SerialPort.OnBeforeClose := @SerialPortBeforeClose;
  SerialPort.OnAfterClose := @SerialPortAfterClose;
  SerialPort.OnError := @SerialPortError;

  // читаем настройки порта из файла:
  SerialPort.LoadSettings(SettingsFileName);
  // Загрузка настроек для форм:
  FormMain.LoadSettings(SettingsFileName);
  FormLog.LoadSettings(SettingsFileName);
end;

procedure TData.AboutMenuItemClick(Sender: TObject);
begin
  // Пункт меню "О программе"
  with TFormAbout.Create(Application) do
    try
      ShowModal;
    finally
      Free;
    end;
end;

procedure TData.ConsoleItemClick(Sender: TObject);
begin
  // Пункт меню "Лог порта"
  if FormLog.Visible then
  begin
    FormLog.Close;
  end
  else
  begin
    FormLog.Show;
  end;
  ConsoleItem.Checked := FormLog.Visible;
  FormMain.LogToolButton.Down := ConsoleItem.Checked;
end;

procedure TData.ExitItemClick(Sender: TObject);
begin
  // Пункт меню "Выход"
  FormMain.Close;
end;

procedure TData.PanelItemClick(Sender: TObject);
begin
  // Пункт "Панель инструментов"
  PanelItem.Checked := not PanelItem.Checked;
  FormMain.MainToolBar.Visible := PanelItem.Checked;
  // сохраняем настройку:
  with TIniFile.Create(SettingsFileName) do
    try
      WriteBool('Settings', 'ShowToolBar', PanelItem.Checked);
    finally
      Free;
    end;
end;

procedure TData.PortItemClick(Sender: TObject);
begin
  // Пункт меню Открыть-Закрыть
  if PortIsOpen then
  begin
    SerialPort.Close;
  end
  else
  begin
    SerialPort.Open;
  end;
end;

procedure TData.PortSetupItemClick(Sender: TObject);
begin
  // Пункт меню "Настройки порта"
  if SetupPort then
  begin
    FormMain.UpdateStatusPanel;
  end;
end;

procedure TData.SetupItemClick(Sender: TObject);
begin
  // Пункт меню настройка программы
  with TFormSetup.Create(Application) do
    try

      // тут будет заполнение настроек

      if ShowModal = mrOk then
      begin

        // тут будет применение настроек

      end;

    finally
      Free;
    end;
end;

procedure TData.SerialPortRxData(Sender: TObject);
var
  i, Count: integer;
  buf: array[0..2047] of byte;
  str: string;
begin
  // событие при поступлении байт в порт
  Count := SerialPort.Read(buf, 2048);

  // buf содержит считанные байты в кол-ве count
  str := '<<';
  for i := 0 to Count - 1 do
  begin
    str := str + ' ' + IntToHex(buf[i], 2);
  end;
  FormLog.AddToLog(str);
end;

procedure TData.SerialPortBeforeOpen(Sender: TObject);
begin
  // событие перед открытием порта

end;

procedure TData.SerialPortAfterOpen(Sender: TObject);
begin
  // событие после открытия порта
  FormLog.AddToLog('Порт открыт.');
  PortItem.Caption := 'Закрыть порт';
  FormMain.MainStatusBar.Panels.Items[3].Text := 'Открыт';
  PortSetupItem.Enabled := False;

  SetupItem.Enabled := PortSetupItem.Enabled;
  FormMain.PortToolButton.Hint := PortItem.Caption;
  FormMain.PortSetupToolButton.Enabled := PortSetupItem.Enabled;
  FormMain.SetupToolButton.Enabled := SetupItem.Enabled;
end;

procedure TData.SerialPortBeforeClose(Sender: TObject);
begin
  // событие перед закрытием порта

end;

procedure TData.SerialPortAfterClose(Sender: TObject);
begin
  // событие после закрытия порта
  FormLog.AddToLog('Порт закрыт.');
  PortItem.Caption := 'Открыть порт';
  FormMain.MainStatusBar.Panels.Items[3].Text := 'Закрыт';
  PortSetupItem.Enabled := True;

  SetupItem.Enabled := PortSetupItem.Enabled;
  FormMain.PortToolButton.Hint := PortItem.Caption;
  FormMain.PortSetupToolButton.Enabled := PortSetupItem.Enabled;
  FormMain.SetupToolButton.Enabled := SetupItem.Enabled;
end;

procedure TData.SerialPortError(Sender: TObject; ErrorCode: integer;
  ErrorMessage: string);
begin
  // Событие при ошибке порта
  FormLog.AddToLog('Ошибка порта ' + IntToStr(ErrorCode) + ': ' + ErrorMessage);
end;

function TData.PortIsOpen: boolean;
begin
  Result := (Assigned(SerialPort) and SerialPort.Connected);
end;

function TData.OpenPort: boolean;
begin
  if not Assigned(SerialPort) then
  begin
    Result := False;
    Exit;
  end;
  SerialPort.Open;
  Result := SerialPort.Connected;
end;

function TData.ClosePort: boolean;
begin
  if not Assigned(SerialPort) then
  begin
    Result := False;
    Exit;
  end;
  SerialPort.Close;
  Result := not SerialPort.Connected;
end;

function TData.SetupPort: boolean;
begin
  Result := SerialPort.SetupPortDialog(SettingsFileName);
end;

function TData.GetPortSettingsStr: string;
begin
  Result := SerialPort.GetSettingsStr;
end;

function TData.WriteToPort(buf: array of byte; Count: integer): integer;
var
  str: string;
  i: integer;
begin
  // запись массива байт в порт
  Result := 0;
  if not SerialPort.Connected then
    Exit;

  Result := SerialPort.Write(buf, Count);

  str := '>>';
  for i := 0 to Count - 1 do
  begin
    str := str + ' ' + IntToHex(buf[i], 2);
  end;
  FormLog.AddToLog(str);
end;

end.
